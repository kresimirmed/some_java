package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) nam služi za vraćanje {@code true} u metodi
 * {@code execute} što daje znak "procesoru" da prekine sa radom (da se ugasi).
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrHalt implements Instruction {

	/**
	 * Metoda ne prima argumente. Provjerava ima li iza naredbe "halt"
	 * argumenata, ako ima baca {@code IllegalArgumentException} inače ne radi
	 * ništa.
	 * 
	 * @param arguments
	 *            Neiskorišteno.
	 */
	public InstrHalt(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Did not expect arguments!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		return true;
	}

}
