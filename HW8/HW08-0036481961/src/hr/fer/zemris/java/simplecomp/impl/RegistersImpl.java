package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred implementira {@code Registers}. Za detaljnjije obješnjenj
 * pogledajte java dokumentaciju sučelja {@link Registers}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class RegistersImpl implements Registers {

	/**
	 * Predstavlja registre našeg računala.
	 */
	private Object[] registers;

	/**
	 * Pregstavlja programsko brojilo.
	 */
	private int counter;

	/**
	 * Predstavlja zastavicu.
	 */
	private boolean flag;

	/**
	 * Konstruktor koji prima broj registara koji će procesoru biti na
	 * raspolaganju, zatim stvara polje {@code Object} primljene veličine
	 * {@code size}
	 * 
	 * @param size
	 *            Broj registara koji nam stoje na raspolaganju.
	 */
	public RegistersImpl(int size) {
		super();
		this.registers = new Object[size];
		this.flag = false;
		this.counter = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getRegisterValue(int index) {
		if (index < 0 || index >= registers.length) {
			throw new IndexOutOfBoundsException("This location do not exist!");
		}
		return registers[index];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRegisterValue(int index, Object value) {
		if (index < 0 || index >= registers.length) {
			throw new IndexOutOfBoundsException("This location do not exist!");
		}
		registers[index] = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getProgramCounter() {
		return counter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setProgramCounter(int value) {
		counter = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void incrementProgramCounter() {
		counter += 1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getFlag() {
		return flag;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFlag(boolean value) {
		flag = value;
	}

}
