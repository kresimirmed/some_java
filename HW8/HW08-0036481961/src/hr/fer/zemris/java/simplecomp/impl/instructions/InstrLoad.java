package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) učitava vrijednost iz memorije u registar. Nije
 * dozvoljeno indirektno adresiranje. Prima dvije indexa za {@code Registar} i
 * {@code Memory} te sa određene adrese u memoriji kopira (učitava) u zadani
 * registar.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrLoad implements Instruction {

	/**
	 * Predstavlja index registra.
	 */
	private int indexRegistra;

	/**
	 * Predstavlja memorisku adresu.
	 */
	private int memoryAddress;

	/**
	 * Kroz listu se primaju i provjravaju argumenti, ako su argumetni
	 * nesipravni baca se {@code IllegalArgumentException}. Uzimaju se
	 * vrijednosti indexa registra i adresa u memoriji i spremaju se u
	 * odgovarajuće argumente. Prvi član u listi nam predstavlja registar dok
	 * drugi memorisku lokaciju.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrLoad(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}
		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch for argument 0!");
		}

		this.indexRegistra = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.memoryAddress = (Integer) arguments.get(1).getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getMemory().getLocation(memoryAddress);
		computer.getRegisters().setRegisterValue(indexRegistra, value);
		return false;
	}

}
