package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovj razred (instrukcija) služi za poziv potprograma u asebleru. Prima jedan argumenat
 * {@code location} koja prestavlja adresu potprograma, zatim na stog stavlja
 * trenutniu adresu koja se izvodi i u {@code counteru} kao trnutnu naredbu
 * postavi naredbu koja se nalazi na mjestu {@code location}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrCall implements Instruction {

	/**
	 * Predstavlja lokaciju programa kojeg pozivamo.
	 */
	private int location;

	/**
	 * Prima jedan element liste,taj element je lokacija potprograma. Ako se
	 * dbojie krivi broj argumenata dogodi se iznimka
	 * {@code IllegalArgumentException}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrCall(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}
		this.location = (Integer) arguments.get(0).getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int value = computer.getRegisters().getProgramCounter();
		int memLocation = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		computer.getMemory().setLocation(memLocation, value);
		--memLocation;
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, memLocation);
		computer.getRegisters().setProgramCounter(location - 1);
		return false;
	}

}
