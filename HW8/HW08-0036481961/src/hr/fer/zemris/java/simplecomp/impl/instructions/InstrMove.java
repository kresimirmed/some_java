package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) služi za kopiranje vrijednosti iz memorije, samoga
 * broja ili iz registra u odabrani registar. Prvi argument može biti samo
 * registar, dok drugi argument može biti registar, memoriska lokacija ili broj.
 * Podržano je indirektno adresiranje.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrMove implements Instruction {

	/**
	 * Prestavlja registar u koji se pohranjuje vrijednost.
	 */
	private int indexRegister;

	/**
	 * Prestavlja argumet instrukcije druge variable.
	 */
	private InstructionArgument otherArgument;

	/**
	 * Prestavlja vrijednost druge variable.
	 */
	private Integer otherArgumentValue;

	/**
	 * Metoda prima dva argumenta, zatim ih provjerava, ako su argumenti
	 * neispravni baca {@code IllegalArgumentException}. U suprotnom izračunava
	 * i posprema vrijednosti u odgovarajuće variable.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrMove(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException("Type mismatch for argument 0!");
		}
		indexRegister = (Integer) arguments.get(0).getValue();
		otherArgument = arguments.get(1);
		otherArgumentValue = (Integer) otherArgument.getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value;
		if (otherArgument.isRegister()) {

			int index = RegisterUtil.getRegisterIndex(otherArgumentValue);

			if (RegisterUtil.isIndirect(otherArgumentValue)) {
				int offset = RegisterUtil.getRegisterOffset(otherArgumentValue);
				int addres = (int) computer.getRegisters().getRegisterValue(index);

				value = computer.getMemory().getLocation(addres + offset);
			} else {
				value = computer.getRegisters().getRegisterValue(index);
			}
		} else {
			if (otherArgument.isNumber()) {
				value = otherArgumentValue;
			} else {
				value = computer.getMemory().getLocation(otherArgumentValue);
			}
		}

		int index = RegisterUtil.getRegisterIndex(indexRegister);

		if (RegisterUtil.isIndirect(indexRegister)) {
			int offset = RegisterUtil.getRegisterOffset(indexRegister);
			int addres = (int) computer.getRegisters().getRegisterValue(index);
			computer.getMemory().setLocation(offset + addres, value);
		} else {
			computer.getRegisters().setRegisterValue(index, value);
		}

		return false;
	}

}
