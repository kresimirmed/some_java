package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) radi uvijetni skok na zadanu adresu. Instrukcija će
 * izvesti skok jedino ako je zastavica {@code flag} postavljena na {@code true}
 * vrijednost, inače skok se neće izvesti.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrJumpIfTrue implements Instruction {

	/**
	 * Predstavlja instrukciju jump.
	 */
	private InstrJump jump;

	/**
	 * Konstruktor inicializira razred (instrukciju) {@code InstrJump}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrJumpIfTrue(List<InstructionArgument> arguments) {
		jump = new InstrJump(arguments);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		if (computer.getRegisters().getFlag()) {
			jump.execute(computer);
		}
		return false;
	}

}
