package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) uspoređuje dvije vrijednosti. Ako je vrijednost
 * jednaka tada se zastavica postavlja na {@code true}, inače na {@code false}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrTestEquals implements Instruction {

	/**
	 * Prvi registar.
	 */
	private int indexRegistra1;

	/**
	 * Drugi registar.
	 */
	private int indexRegistra2;

	/**
	 * Prima dvije vrijednosti, provjerava ih, te baca
	 * {@code IllegalArgumentException} ako je potrebno (došlo do greške). Inače
	 * postavlja vrijednosti u članske variable za daljnju usporedbu.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrTestEquals(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}
		for (int i = 0; i < 2; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Type mismatch for argument " + i + "!");
			}
		}
		this.indexRegistra1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.indexRegistra2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters().getRegisterValue(indexRegistra1);
		Object value2 = computer.getRegisters().getRegisterValue(indexRegistra2);
		boolean flag = false;
		if (value1.equals(value2)) {
			flag = true;
		}
		computer.getRegisters().setFlag(flag);
		return false;
	}

}
