package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred nam služi za pohranu memorije i registara za trenutni program
 * koji se izvodi na "računalu", odnosno predstavalj nam podatkovni dio
 * računala. On implementira {@code Computer}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ComputerImpl implements Computer {

	/**
	 * Predstavlja registre računala.
	 */
	private Registers registers;

	/**
	 * Predstavlja memoriju računala.
	 */
	private Memory memory;

	/**
	 * Stvara računalo s {@code registrsSize} registara i {@code memorySize}
	 * memoriskih lokacija.
	 * 
	 * @param registersSize
	 *            Broj registara.
	 * @param memorySize
	 *            Broj memoriskih lokacija.
	 */
	public ComputerImpl(int memorySize, int registersSize) {
		super();
		this.registers = new RegistersImpl(registersSize);
		this.memory = new MemoryImpl(memorySize);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Registers getRegisters() {
		return registers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Memory getMemory() {
		return memory;
	}

}
