package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) nam množi dvije vrijednosti u registrima i posprema
 * ih u prvi ergistar. Prima tri argumenta koji prestavljaju indexe registara,
 * zatim množi drugu i treću vrijednost u registrima i to sprema u registar koji
 * s enalazi na nultoj lokaciji u predanoj listi. Ne podržava se indirektno
 * adresiranje.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrMul implements Instruction {

	/**
	 * Index registra u koji se sprema rezultat.
	 */
	private int indexRegistra1;

	/**
	 * Index registra u kojem se nalazi jedan množitelj.
	 */
	private int indexRegistra2;

	/**
	 * Index registra u kojem se nalazi drugi množitelj.
	 */
	private int indexRegistra3;

	/**
	 * Konstruktor provjerava ispravnost primljenih argumenata, ako su argumenti
	 * neispravni baca se {@code IllegalArgumentException}. Ako su argumetni
	 * ispravni sprema indexe registara u određee članske varijable.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrMul(List<InstructionArgument> arguments) {
		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}
		for (int i = 0; i < 3; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Type mismatch for argument " + i + "!");
			}
		}
		this.indexRegistra1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.indexRegistra2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
		this.indexRegistra3 = RegisterUtil.getRegisterIndex((Integer) arguments.get(2).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters().getRegisterValue(indexRegistra2);
		Object value2 = computer.getRegisters().getRegisterValue(indexRegistra3);
		computer.getRegisters().setRegisterValue(indexRegistra1, Integer.valueOf((Integer) value1 * (Integer) value2));
		return false;
	}

}
