package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) služi kao umanjitelj vrijednosi registra za 1.
 * Prima jedan regisrat čiju vrijednost umanjuje za 1, te tu umanjenu vrijednost
 * pohranjuje u isti registar. Ova instrukcija ne podržava indirektno
 * adresiranje.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrDecrement implements Instruction {

	/**
	 * Registar u kojem se nalazi vrijednost koja se umanjuje.
	 */
	private int indexRegistra;

	/**
	 * Konstruktor koji prima listu argumenata u kojoj se treba nalaziti samo
	 * jedan argument. Provjerava jeli se u listi nalazi zadovoljavajuž broj
	 * argumenata i jeli li argumenti odgovarajući, ako argumenti nisu
	 * odgovarajući i ne odgovara broj argumenata baca se
	 * {@code IllegalArgumentException}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrDecrement(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}
		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch for argument!");
		}
		this.indexRegistra = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getRegisters().getRegisterValue(indexRegistra);
		computer.getRegisters().setRegisterValue(indexRegistra, Integer.valueOf(((Integer) value) - 1));
		return false;
	}

}
