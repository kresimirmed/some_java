package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) izvodi bezuvjetan skok na predanu joj adresu.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrJump implements Instruction {

	/**
	 * Lokacija naredbe na koju "skačemo".
	 */
	private int lokacija;

	/**
	 * Provjerava broj primljenih argumenata. Očekuje se da će se u listi samo
	 * nalaziti lokacija na koju skačemo, u suprotnom slučaju baciti će se
	 * {@code IllegalArgumentException}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrJump(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}

		this.lokacija = (Integer) arguments.get(0).getValue();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		computer.getRegisters().setProgramCounter(lokacija - 1);
		return false;
	}

}
