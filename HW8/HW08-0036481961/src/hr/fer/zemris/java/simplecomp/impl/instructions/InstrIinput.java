package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) služi za unos podataka sa tipkovnice u odabranu
 * lokaciju u memoriji. Unos prihvaća unosi jedino {@code Integer} tipa
 * podataka. Ako smo unijeli {@code Integer} tip tada zastavicu {@code flag}
 * postavlja na {@code true}, inače ako se dogodi bilo kakva greška ili se ne
 * učita tip {@code Integer} tada se zastavica postavlja na {@code false}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrIinput implements Instruction {

	/**
	 * Lokacija gdje spremamo učitani niz.
	 */
	private int location;

	/**
	 * Služi za čitanje s tipkovnice.
	 */
	BufferedReader reader;

	/**
	 * Konstruktor prima lokaciju gdje se treba upisati učitani niz. Lokaciju
	 * zapisa u memoriji prima kako jedini član predane liste {@code arguments}.
	 * Moramo primiti točno jedan član u listi inače se baca
	 * {@code IllegalArgumentException}. Zatim inicializira {@code reader} i
	 * {@code location} koji postavlja na adresu u memoriji.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrIinput(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}

		location = (Integer) arguments.get(0).getValue();
		reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		try {
			String line = reader.readLine().trim();
			if (isInteger(line)) {
				computer.getMemory().setLocation(location, Integer.parseInt(line));
				computer.getRegisters().setFlag(true);
			} else {
				computer.getRegisters().setFlag(false);
			}
		} catch (IOException e) {
			computer.getRegisters().setFlag(false);
		}
		return false;
	}

	/**
	 * Provjerava jeli se predani {@code String} može pretvoriti u
	 * {@code Integer} tip.
	 * 
	 * @param line
	 *            Niz znakova.
	 * @return {@code true} ako se {@code line} može prikazati kao
	 *         {@code Integer} tip, inače {@code false}.
	 */
	private boolean isInteger(String line) {
		try {
			Integer.parseInt(line);
		} catch (NumberFormatException e) {
			return false;
		}

		return true;

	}

}
