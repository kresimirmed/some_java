package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja instrukciju add našeg procesora. Prima tri registra,
 * ako ne primi tri argumenta baca iznimku {@code IllegalArgumentException}.
 * Prvi registar služi za pohranu, a u druga dva se nalaze vrijednosti koje
 * zbrajamo. Ova instrukcija ne podržava indirektno adresiranje.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrAdd implements Instruction {

	/**
	 * Registar u koji se sprema vrijedijednost zbroja.
	 */
	private int indexRegistra1;

	/**
	 * Registar u kojemu se nalazi prvi pribrojnik.
	 */
	private int indexRegistra2;

	/**
	 * Registar u kojemu se nalazi drugi pribrojnik.
	 */
	private int indexRegistra3;

	/**
	 * Konstruktor koji prima listu argumenata. Provjerava vrijednosti liste
	 * jesu li registri, zatim ih sprema u odgovarajuće članske variable.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrAdd(List<InstructionArgument> arguments) {
		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}
		for (int i = 0; i < 3; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Type mismatch for argument " + i + "!");
			}
		}
		this.indexRegistra1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.indexRegistra2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
		this.indexRegistra3 = RegisterUtil.getRegisterIndex((Integer) arguments.get(2).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters().getRegisterValue(indexRegistra2);
		Object value2 = computer.getRegisters().getRegisterValue(indexRegistra3);
		computer.getRegisters().setRegisterValue(indexRegistra1, Integer.valueOf((Integer) value1 + (Integer) value2));
		return false;
	}

}
