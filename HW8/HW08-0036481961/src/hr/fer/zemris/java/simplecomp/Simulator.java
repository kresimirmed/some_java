package hr.fer.zemris.java.simplecomp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import hr.fer.zemris.java.simplecomp.impl.*;
import hr.fer.zemris.java.simplecomp.models.*;
import hr.fer.zemris.java.simplecomp.parser.*;

/**
 * Ovaj razred služi za simulaciju rada našeg procesora.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Simulator {

	/**
	 * Ova metoda se pokreće s ovim razredom.
	 * 
	 * @param args
	 *            Neiskorišteno.
	 * @throws Exception
	 *             Propisuje nam parser.
	 */
	public static void main(String[] args) throws Exception {
		File file = null;
		if (args.length > 1) {
			System.err.println("Expected 0 or 1 arguments!");
			System.exit(-1);
		} else if (args.length == 1) {
			file = new File(args[0]);
		} else {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));
			System.out.printf("Unesite put do dateke -> ");
			file = new File(reader.readLine().trim());
		}
		if (!file.exists() || !file.isFile()) {
			System.err.println("Sljedeca datoteka ne postoji ili nije datoteka:\n " + file.getAbsolutePath());
			System.exit(-1);
		}
		try {
			Computer comp = new ComputerImpl(256, 16);
			InstructionCreator creator = new InstructionCreatorImpl("hr.fer.zemris.java.simplecomp.impl.instructions");
			ProgramParser.parse(file.getAbsolutePath(), comp, creator);
			ExecutionUnit exec = new ExecutionUnitImpl();
			exec.go(comp);
		} catch (Exception e) {
			System.err.println(e);
			System.exit(-1);
		}
	}

}
