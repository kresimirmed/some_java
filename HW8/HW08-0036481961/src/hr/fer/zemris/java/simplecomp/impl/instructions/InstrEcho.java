package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred (instrukcija) uzima sadržaj registra rX i ispisuje ga na ekran (pozivom metode
 * System.out.print()). Ovaj razred podržava indirektno adresiranje (legalan
 * oblik instrukcije je i echo [rX+offset]).
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrEcho implements Instruction {

	/**
	 * Prestavlja argument instrukcije.
	 */
	private InstructionArgument firstArgument;

	/**
	 * Konstruktor prima listu argumenata koja sadrži samo jedan element koji
	 * sadrži registar koji ispisujemo na ekran. Zatim samo taj argument
	 * pospremimo u {@code firstArgument}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrEcho(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException("Type mismatch for argument!");
		}
		firstArgument = arguments.get(0);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int indexRegistra = RegisterUtil.getRegisterIndex((Integer) firstArgument.getValue());

		if (RegisterUtil.isIndirect((Integer) firstArgument.getValue())) {
			int offset = RegisterUtil.getRegisterOffset((Integer) firstArgument.getValue());
			int addres = (Integer) computer.getRegisters().getRegisterValue(indexRegistra);
			Object value = computer.getMemory().getLocation(offset + addres);
			System.out.print(value);
		} else {
			Object value = computer.getRegisters().getRegisterValue(indexRegistra);
			System.out.print(value);
		}
		return false;
	}

}
