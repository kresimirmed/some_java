package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.ExecutionUnit;
import hr.fer.zemris.java.simplecomp.models.Instruction;

/**
 * Ovaj razred implementira {@code ExecutionUnit}.Predstavlja upravljački sklop
 * računala. Ovaj razred "izvodi" program odnosno predstavlja impulse takta za
 * sam procesor.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ExecutionUnitImpl implements ExecutionUnit {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean go(Computer computer) {
		computer.getRegisters().setProgramCounter(0);
		while (true) {
			Object proc = computer.getMemory().getLocation(computer.getRegisters().getProgramCounter());
			boolean halt = ((Instruction) proc).execute(computer);
			if (halt) {
				break;
			}
			computer.getRegisters().setProgramCounter(computer.getRegisters().getProgramCounter() + 1);
		}
		return true;
	}

}
