package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred (instrukcija) služi za povrat iz potprograma. Ona sa stoga uzima
 * vrijednost (lokaciju) pozivajuće naredbe i programsko brojilo postavlja na
 * adresu pozivajuće naredbe.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrRet implements Instruction {

	/**
	 * Konstruktor provjerava jedino jeli ima argumenata u listi (jeli još nešto
	 * napisano iza naredbe ret). Ako ima nešto baca
	 * {@code IllegalArgumentException}, inače ne radi ništa.
	 * 
	 * @param arguments
	 *            Ne koristimo.
	 */
	public InstrRet(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Did not expect arguments!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int memLocation = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		++memLocation;
		int value = (Integer) computer.getMemory().getLocation(memLocation);
		computer.getRegisters().setProgramCounter(value);
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, memLocation);
		return false;
	}
}
