package hr.fer.zemris.java.simplecomp;

/**
 * Ovaj razred služi za određivanje index registara, jeli adresiranje indirektno
 * i vadi pomak koji je potrebno koristiti.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class RegisterUtil {

	/**
	 * Is predanog deskriptora vadi index registra.
	 * 
	 * @param registerDescriptor
	 *            Predani deskriptor.
	 * @return Index registra.
	 */
	public static int getRegisterIndex(int registerDescriptor) {
		return registerDescriptor & 0xFF;
	}

	/**
	 * Iz predanog deskriptora određuje je li se radi o indirektnom adresiranju.
	 * 
	 * @param registerDescriptor
	 *            Predani deskriptor.
	 * @return {@code true} ako je adresiranje indirektno, inače {@code false}.
	 */
	public static boolean isIndirect(int registerDescriptor) {
		if (((registerDescriptor >> 24) & 1) == 1) {
			return true;
		}
		return false;
	}

	/**
	 * Metoda iz predanog deskriptora vadi pomak koji treba koristiti.
	 * 
	 * @param registerDescriptor
	 *            Predani deskriptor.
	 * @return Pomak.
	 */
	public static int getRegisterOffset(int registerDescriptor) {
		if (!isIndirect(registerDescriptor)) {
			return 0;
		}
		return (short) ((registerDescriptor >> 8));
	}
}
