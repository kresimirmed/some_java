package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred (instrukcija) nam služi za skidanje zadnjeg elementa koji je
 * stavljen na stog. Prima jedan argument koji prestavlja registar u koji ćemo
 * pohraniti vrijednost koja se nalazi na stogu. Ne podržava se indirektno
 * adresiranje.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrPop implements Instruction {

	/**
	 * Index registra u koji pohranjujemo vrijednost.
	 */
	private int indexRegistra;

	/**
	 * Prima jedan argument u koji će se pohraniti vrijednost koja se nalazi na
	 * vrhu stoga. Ako je argument neispravan konstruktor baca
	 * {@code IllegalArgumentException}.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrPop(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}
		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch for argument!");
		}
		this.indexRegistra = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int memLocation = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		++memLocation;
		Object value = computer.getMemory().getLocation(memLocation);
		computer.getRegisters().setRegisterValue(indexRegistra, value);
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, memLocation);
		return false;
	}

}
