package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred (instrukcija) služi za pohranjivanje vrijednosti iz registra na
 * vrh stoga. Indirektno adresiranje niije podržano.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class InstrPush implements Instruction {

	/**
	 * Registar iz kojeg se pohranjuje vrijednost.
	 */
	private int indexRegistra;

	/**
	 * Prima samo jedan argument, koji provjerava. Ako primljeni argument nije
	 * ispravan baca {@code IllegalArgumentException}, inače posprema vrijednost
	 * primljenog argumenta u člansku variablu.
	 * 
	 * @param arguments
	 *            Lista argumenata.
	 */
	public InstrPush(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 arguments!");
		}
		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch for argument!");
		}
		this.indexRegistra = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getRegisters().getRegisterValue(indexRegistra);
		int memLocation = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		computer.getMemory().setLocation(memLocation, value);
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, --memLocation);
		return false;
	}
}
