package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Memory;

/**
 * Ovaj razred implementira {@code Memory}. Za detaljnjije obješnjenj pogledajte
 * java dokumentaciju sučelja {@link Memory}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class MemoryImpl implements Memory {

	/**
	 * Predstavlja memoriju našeg računala.
	 */
	private Object[] memory;

	/**
	 * Konstruktor koji primabroj memorijskih lokacija (tj. veličinu memorije),
	 * zatim stvara polje {@code Object} primljene veličine {@code size}
	 * 
	 * @param size
	 *            Veličina memorije
	 */
	public MemoryImpl(int size) {
		super();
		this.memory = new Object[size];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLocation(int location, Object value) {
		if (location < 0 || location >= memory.length) {
			throw new IndexOutOfBoundsException("This location do not exist!");
		}
		memory[location] = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getLocation(int location) {
		if (location < 0 || location >= memory.length) {
			throw new IndexOutOfBoundsException("This location do not exist!");
		}
		return memory[location];
	}

}
