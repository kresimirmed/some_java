package hr.fer.zemris.java.simplecomp.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import java.util.List;

import hr.fer.zemris.java.simplecomp.impl.instructions.*;
import hr.fer.zemris.java.simplecomp.models.*;

@SuppressWarnings("javadoc")
@RunWith(MockitoJUnitRunner.class)
public class Tests {

	@Mock
	private Registers reg;
	@Mock
	private Memory mem;
	@Mock
	private Computer comp;
	@Mock
	private List<InstructionArgument> arguments;
	@Mock
	InstructionArgument arg0;
	@Mock
	InstructionArgument arg1;

	@Before
	public void setUp() {

		when(arg0.isRegister()).thenReturn(true);
		when(arg1.isRegister()).thenReturn(true);

		when(arg0.getValue()).thenReturn((Integer) 10);
		when(arg1.getValue()).thenReturn((Integer) 11);

		when(comp.getMemory()).thenReturn(mem);
		when(comp.getRegisters()).thenReturn(reg);

		when(mem.getLocation(anyInt())).thenReturn(2);

		when(reg.getFlag()).thenReturn(false);
		when(reg.getProgramCounter()).thenReturn(5);
		when(reg.getRegisterValue(anyInt())).thenReturn(10);

	}

	@Test
	public void Test1Move() {
		when(arguments.size()).thenReturn(2);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		InstrMove move = new InstrMove(arguments);
		move.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg1, times(1)).isRegister();
		verify(arg0, times(1)).getValue();
		verify(arg1, times(1)).getValue();
		verify(comp, times(2)).getRegisters();
		verify(comp, times(0)).getMemory();

	}
	
	@Test
	public void Test1Move2() {
		when(arg0.getValue()).thenReturn((Integer) 0xFFFFFFF);
		when(arguments.size()).thenReturn(2);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		InstrMove move = new InstrMove(arguments);
		move.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg1, times(1)).isRegister();
		verify(arg0, times(1)).getValue();
		verify(arg1, times(1)).getValue();
		verify(comp, times(2)).getRegisters();
		verify(comp, times(1)).getMemory();

	}
	
	@Test
	public void Test1Move3() {
		when(arg0.getValue()).thenReturn((Integer) 0xFFFFFFF);
		when(arg1.getValue()).thenReturn((Integer) 0xFFFFFFF);
		when(arguments.size()).thenReturn(2);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		InstrMove move = new InstrMove(arguments);
		move.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg1, times(1)).isRegister();
		verify(arg0, times(1)).getValue();
		verify(arg1, times(1)).getValue();
		verify(comp, times(2)).getRegisters();
		verify(comp, times(2)).getMemory();

	}
	

	@Test(expected = IllegalArgumentException.class)
	public void Test2Move() {
		when(arguments.size()).thenReturn(3);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrMove(arguments);

	}

	@Test(expected = IllegalArgumentException.class)
	public void Test3Move() {
		when(arguments.size()).thenReturn(2);
		when(arg0.isRegister()).thenReturn(false);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrMove(arguments);

	}

	@Test
	public void Test4Load() {
		when(arguments.size()).thenReturn(2);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		InstrLoad load = new InstrLoad(arguments);
		load.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg1, times(0)).isRegister();
		verify(arg0, times(2)).getValue();
		verify(arg1, times(1)).getValue();
		verify(comp, times(1)).getRegisters();
		verify(comp, times(1)).getMemory();
		verify(reg, times(0)).getRegisterValue(anyInt());
		verify(mem, times(1)).getLocation(anyInt());
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test5Load() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrLoad(arguments);
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test6Load() {
		when(arguments.size()).thenReturn(2);
		when(arg0.isRegister()).thenReturn(false);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrLoad(arguments);
	}

	@Test
	public void Test7Push() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);

		arguments.add(arg0);

		InstrPush push = new InstrPush(arguments);
		push.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg0, times(2)).getValue();
		verify(comp, times(3)).getRegisters();
		verify(comp, times(1)).getMemory();
		verify(reg, times(2)).getRegisterValue(anyInt());
		verify(mem, times(1)).setLocation(anyInt(), anyInt());
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test8Push() {
		when(arguments.size()).thenReturn(3);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrPush(arguments);
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test9Push() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);

		when(arg0.isRegister()).thenReturn(false);

		arguments.add(arg0);

		new InstrPush(arguments);
	}

	@Test
	public void Test10Pop() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);

		arguments.add(arg0);

		InstrPop pop = new InstrPop(arguments);
		pop.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(1)).isRegister();
		verify(arg0, times(2)).getValue();
		verify(comp, times(3)).getRegisters();
		verify(comp, times(1)).getMemory();
		verify(reg, times(1)).getRegisterValue(anyInt());
		verify(mem, times(1)).getLocation(anyInt());
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test11Pop() {
		when(arguments.size()).thenReturn(3);
		when(arguments.get(0)).thenReturn(arg0);
		when(arguments.get(1)).thenReturn(arg1);

		arguments.add(arg0);
		arguments.add(arg1);

		new InstrPop(arguments);
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test12Pop() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);

		when(arg0.isRegister()).thenReturn(false);

		arguments.add(arg0);

		new InstrPop(arguments);
	}

	@Test
	public void Test13Call() {
		when(arguments.size()).thenReturn(1);
		when(arguments.get(0)).thenReturn(arg0);

		arguments.add(arg0);

		InstrCall call = new InstrCall(arguments);
		call.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(0)).isRegister();
		verify(arg0, times(1)).getValue();
		verify(comp, times(4)).getRegisters();
		verify(comp, times(1)).getMemory();
		verify(reg, times(1)).getRegisterValue(anyInt());
		verify(reg, times(1)).getProgramCounter();
		verify(reg, times(1)).setProgramCounter(anyInt());
		verify(mem, times(1)).setLocation(anyInt(), anyInt());
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test14Call() {
		when(arguments.size()).thenReturn(0);
		when(arguments.get(0)).thenReturn(arg0);

		arguments.add(arg0);

		new InstrCall(arguments);
	}

	@Test(expected = IllegalArgumentException.class)
	public void Test15Call() {
		when(arguments.size()).thenReturn(3);
		when(arguments.get(0)).thenReturn(arg0);

		arguments.add(arg0);

		new InstrCall(arguments);
	}

	@Test
	public void Test16Ret() {
		when(arguments.size()).thenReturn(0);

		InstrRet ret = new InstrRet(arguments);
		ret.execute(comp);

		verify(arguments, times(1)).size();
		verify(arg0, times(0)).isRegister();
		verify(arg0, times(0)).getValue();
		verify(comp, times(3)).getRegisters();
		verify(comp, times(1)).getMemory();
		verify(reg, times(1)).getRegisterValue(anyInt());
		verify(reg, times(0)).getProgramCounter();
		verify(reg, times(1)).setProgramCounter(anyInt());
		verify(mem, times(1)).getLocation(anyInt());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Test17Ret() {
		when(arguments.size()).thenReturn(2);

		new InstrRet(arguments);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void Test18Ret() {
		when(arguments.size()).thenReturn(1);

		new InstrRet(arguments);
	}
}
