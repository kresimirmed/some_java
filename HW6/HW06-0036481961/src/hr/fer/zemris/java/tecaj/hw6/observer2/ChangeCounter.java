package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class count (and writes to the standard output) the number of times
 * value stored integer (in {@code IntegerStorage}) has been changed since the
 * registration.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ChangeCounter implements IntegerStorageObserver {

	/**
	 * Represent counter
	 */
	private int counter;

	@Override
	public void valueChanged(IntegerStorageChange intStorageChange) {
		++counter;
		System.out.println("Number of value changed since tracking: " + counter);
	}
}
