package hr.fer.zemris.java.tecaj.hw6.demo3;

import java.util.Iterator;

/**
 * This implementing interface allows to be the target of the "for-each loop"
 * statement for first n prime number.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class PrimesCollection implements Iterable<Integer> {

	/**
	 * Default first prime number.
	 */
	private final static int FIRST_PRIME = 2;

	/**
	 * Number of the first n primes number.
	 */
	private int numberOfPrime;

	/**
	 * Constructor received how many prime number will be.
	 * 
	 * @param numberOfPrime
	 *            Number first n prime number.
	 */
	public PrimesCollection(int numberOfPrime) {
		if (numberOfPrime < 1) {
			throw new IllegalArgumentException("Number of elements must be greater then 1!");
		}
		this.numberOfPrime = numberOfPrime;
	}

	/**
	 * Returns an iterator over elements of type Integer.
	 */
	public Iterator<Integer> iterator() {
		return new MyIterator();
	}

	private class MyIterator implements Iterator<Integer> {

		/**
		 * Current prime number.
		 */
		private int current = FIRST_PRIME;

		/**
		 * How many prime number is left.
		 */
		private int size = numberOfPrime;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return size > 0;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Integer next() {

			int returnValue = current;
			current = nextPrime();
			--size;
			return returnValue;
		}

		/**
		 * Method return next prime number.
		 * 
		 * @return Prime number.
		 */
		private int nextPrime() {
			int number = current + 1;
			while (true) {
				if (isPrime(number)) {
					return number;
				}
				++number;
			}
		}

		/**
		 * Method check is number prime.
		 * 
		 * @param number
		 *            Number for check.
		 * @return true if number is prime, otherwise false.
		 */
		private boolean isPrime(int number) {
			for (int i = 2; i <= Math.sqrt(number); ++i) {
				if (number % i == 0)
					return false;
			}
			return true;
		}

	}
}
