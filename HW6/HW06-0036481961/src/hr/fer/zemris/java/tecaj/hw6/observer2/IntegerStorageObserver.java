package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This is interface for observer.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public interface IntegerStorageObserver {

	/**
	 * Method which provided what observer work on current changes.
	 * 
	 * @param intStorageChange
	 *            Observed object.
	 */
	public void valueChanged(IntegerStorageChange intStorageChange);

}
