package hr.fer.zemris.java.tecaj.hw6.demo5;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * In this class we read document studenti.txt in memory and convert every line
 * in instance of {@code StudentRecord}. We use stream classes to support
 * functional-style operations on streams of elements, such as map-reduce
 * transformations on collections, in this case on {@code StudentRecord}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class StudentDemo {

	/**
	 * Comparator for student points.
	 */
	public static final Comparator<StudentRecord> COMPARE_POINTS = (z1, z2) -> z2.comparePoints(z1);

	/**
	 * Comparator for student jmbag.
	 */
	public static final Comparator<StudentRecord> COMPARE_JMBAG = (z1, z2) -> z2.compareJMBAG(z1);

	/**
	 * Negative grade.
	 */
	public static final int NEGATIV_GRADE = 1;

	/**
	 * This is main method, it start when somebody start (call) this class.
	 * 
	 * @param args
	 *            Unused
	 * @throws IOException
	 *             If an I/O error occurs reading from the file or a malformed
	 *             or unmappable byte sequence is read.
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {

		List<String> lines = Files.readAllLines(Paths.get("./studenti.txt"), StandardCharsets.UTF_8);
		List<StudentRecord> record = convert(lines);

		long broj = record.stream().filter(s -> s.getmExam() + s.getzExam() + s.getLab() > 25).count();

		long broj5 = record.stream().filter(s -> s.getGrade() == 5).count();

		List<StudentRecord> odlikasi = record.stream().filter(s -> s.getGrade() == 5).collect(Collectors.toList());

		List<StudentRecord> odlikasiSortirani = record.stream().filter(s -> s.getGrade() == 5).sorted(COMPARE_POINTS)
				.collect(Collectors.toList());

		List<String> nepolozeniJMBAGovi = record.stream().filter(s -> s.getGrade() == 1).sorted(COMPARE_JMBAG)
				.map(s -> s.getJmbag()).collect(Collectors.toList());

		Map<Integer, List<StudentRecord>> mapaPoOcjenama = record.stream()
				.collect(Collectors.groupingBy(StudentRecord::getGrade));

		Map<Integer, Integer> mapaPoOcjenama2 = record.stream()
				.collect(Collectors.toMap(StudentRecord::getGrade, e -> 1, Integer::sum));

		Map<Boolean, List<StudentRecord>> prolazNeprolaz = record.stream()
				.collect(Collectors.partitioningBy(s -> s.getGrade() > NEGATIV_GRADE));

	}

	/**
	 * This method convert lines from document (studenti.txt) in to list of
	 * {@code StudentRecord}.
	 * 
	 * @param lines
	 *            List of String what represent lines in document.
	 * @return New list of {@code StudentRecord}.
	 */
	private static List<StudentRecord> convert(List<String> lines) {
		List<StudentRecord> students = new ArrayList<>();

		for (String line : lines) {
			if (line.isEmpty()) {
				break;
			}
			String[] x = line.split("\\s+");
			students.add(new StudentRecord(x[0], x[1], x[2], Double.parseDouble(x[3]), Double.parseDouble(x[4]),
					Double.parseDouble(x[5]), Integer.parseInt(x[6])));
		}

		return students;
	}

}
