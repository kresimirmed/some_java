package hr.fer.zemris.java.tecaj.hw6.demo5;

/**
 * Instances of this class will represent records for each student. Assume that
 * there can not exist multiple records for the same student. Every student
 * record have own unique jmbag, last name, first name, points of first and
 * second exam, points of lab and final grade.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class StudentRecord {

	/**
	 * Student jmbag.
	 */
	private String jmbag;

	/**
	 * Student last name.
	 */
	private String lastName;

	/**
	 * Student first name.
	 */
	private String firstName;

	/**
	 * Points of first exam.
	 */
	private double mExam;

	/**
	 * Points of second exam.
	 */
	private double zExam;

	/**
	 * Lab points.
	 */
	private double lab;

	/**
	 * Student final grade.
	 */
	private int grade;

	/**
	 * Constructor receive all information about student and create one instance
	 * of {@code StudentRecord}.
	 * 
	 * @param jmbag
	 *            Student jmbag.
	 * @param lastName
	 *            Student last name.
	 * @param firstName
	 *            Student first name.
	 * @param mExam
	 *            Points of first exam.
	 * @param zExam
	 *            Points of second exam.
	 * @param lab
	 *            Lab points.
	 * @param grade
	 *            Student mark.
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, double mExam, double zExam, double lab,
			int grade) {
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.mExam = mExam;
		this.zExam = zExam;
		this.lab = lab;
		this.grade = grade;
	}

	/**
	 * Getter for jmbag.
	 * 
	 * @return jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Getter for last name.
	 * 
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Getter for first name.
	 * 
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Getter for points of first exam
	 * 
	 * @return mExam
	 */
	public double getmExam() {
		return mExam;
	}

	/**
	 * Getter for points of second exam.
	 * 
	 * @return zExam
	 */
	public double getzExam() {
		return zExam;
	}

	/**
	 * Getter for lab points.
	 * 
	 * @return lab
	 */
	public double getLab() {
		return lab;
	}

	/**
	 * Getter for student final grade.
	 * 
	 * @return grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * Compare all points of two students. Return {@code 1} if points of this
	 * student greater then point of second student, {@code 0} if points are
	 * equal, otherwise -1.
	 * 
	 * @param s
	 *            Second student
	 * @return 1, 0 or -1
	 */
	public int comparePoints(StudentRecord s) {
		return Math.round((float) Math.signum(lab + mExam + zExam - (s.lab + s.mExam + s.zExam)));
	}

	/**
	 * Compare two student jmbag lexicographically.
	 * 
	 * @param s
	 *            Second student.
	 * @return the value {@code 0} if the argument string is equal to this
	 *         string; a value less than {@code 0} if this string is
	 *         lexicographically less than the string argument; and a value
	 *         greater than {@code 0} if this string is lexicographically
	 *         greater than the string argument.
	 */
	public int compareJMBAG(StudentRecord s) {
		return jmbag.compareTo(s.jmbag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return String.format("(%s) %s, %s -> %4.2f   %4.2f   %4.2f  %3d", jmbag, lastName, firstName, mExam, zExam, lab,
				grade);
	}

}
