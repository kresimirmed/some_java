package hr.fer.zemris.java.tecaj.hw6.observer2;

import java.util.ArrayList;
import java.util.List;

/**
 * Integer storage represent one subject. For every change he inform all
 * observers from list {@code observers}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class IntegerStorage {

	private int value;
	private List<IntegerStorageObserver> observers;

	/**
	 * Constructor.
	 * 
	 * @param initialValue
	 *            Starting value of class variable {@code value}.
	 */
	public IntegerStorage(int initialValue) {
		observers = new ArrayList<>();
		this.value = initialValue;
	}

	/**
	 * This method add observer in observers, only if observer not already there
	 * in list.
	 * 
	 * @param observer
	 */
	public void addObserver(IntegerStorageObserver observer) {
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present. If this list does not contain the element, it is
	 * unchanged.
	 * 
	 * @param observer
	 *            Element to be removed from this list, if present.
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		observers.remove(observer);
	}

	/**
	 * Remove all observers from observers list.
	 */
	public void clearObservers() {
		observers.clear();
	}

	/**
	 * Getters for value.
	 * 
	 * @return value.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * This is modified setter for value.He set {@code value} at received value,
	 * only if new value is different then the current value. After that notify
	 * all registered observers.
	 * 
	 * @param value
	 *            new value.
	 */
	public void setValue(int value) {
		if (this.value != value) {
			IntegerStorageChange intStorageChange = new IntegerStorageChange(this, this.value, value);
			this.value = value;
			if (observers != null) {
				List<IntegerStorageObserver> helpList = new ArrayList<>(observers);
				for (IntegerStorageObserver observer : helpList) {
					observer.valueChanged(intStorageChange);
				}
				helpList.clear();
			}
			intStorageChange = null;
		}
	}

}
