package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This class write a square of the integer stored in the {@code IntegerStorage}
 * to the standard output.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class SquareValue implements IntegerStorageObserver {

	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Provided new value: " + istorage.getValue() + ", squere is "
				+ Math.pow((double) istorage.getValue(), 2));
	}

}
