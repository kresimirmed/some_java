package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * This serves to demonstrate {@code PrimesCollection}. This is second
 * demonstration.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class PrimesDemo2 {

	/**
	 * This is main method it start when somebody call this class.
	 * 
	 * @param args
	 *            Unused
	 */
	public static void main(String[] args) {

		PrimesCollection primesCollection = new PrimesCollection(2);
		for (Integer prime : primesCollection) {
			for (Integer prime2 : primesCollection) {
				System.out.println("Got prime pair: " + prime + ", " + prime2);
			}
		}

	}

}
