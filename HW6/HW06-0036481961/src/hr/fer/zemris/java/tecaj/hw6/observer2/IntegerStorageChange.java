package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class should encapsulate (as read-only properties) following
 * information: reference to IntegerStorage, value before the change has
 * occurred and the new value of current stored object.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class IntegerStorageChange {

	private IntegerStorage intStorage;
	private int oldValue;
	private int newValue;

	/**
	 * Constructor which received {@code IntegerStorage} reference, old value
	 * and new value in {@code IntegerStorage}.
	 * 
	 * @param intStorage
	 *            Reference to {@code IntegerStorage} class.
	 * @param oldValue
	 *            Old value which was saved in {@code IntegerStorage} class.
	 * @param newValue
	 *            New value in {@code IntegerStorage} class.
	 */
	public IntegerStorageChange(IntegerStorage intStorage, int oldValue, int newValue) {
		super();
		this.intStorage = intStorage;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	/**
	 * Getter for reference on {@code IntegerStorage}.
	 * 
	 * @return Reference to {@code IntegerStorage}.
	 */
	public IntegerStorage getIntStorage() {
		return intStorage;
	}

	/**
	 * Getter for oldValue.
	 * 
	 * @return oldValue.
	 */
	public int getOldValue() {
		return oldValue;
	}

	/**
	 * Getter for newValue.
	 * 
	 * @return newValue.
	 */
	public int getNewValue() {
		return newValue;
	}

}
