package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * This serves to demonstrate {@code PrimesCollection}. This is first
 * demonstration.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class PrimesDemo1 {

	/**
	 * This is main method it start when somebody call this class.
	 * 
	 * @param args
	 *            Unused
	 */
	public static void main(String[] args) {

		PrimesCollection primesCollection = new PrimesCollection(5);
		for (Integer prime : primesCollection) {
			System.out.println("Got prime: " + prime);
		}

	}

}
