package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This is example for observations.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ObserverExample {

	/**
	 * This is main method, what contains example.
	 * 
	 * @param args
	 *            Unused
	 */
	public static void main(String[] args) {

		IntegerStorage istorage = new IntegerStorage(20);
		IntegerStorageObserver observer1 = new SquareValue();
		IntegerStorageObserver observer2 = new DoubleValue(5);
		IntegerStorageObserver observer3 = new ChangeCounter();

		istorage.addObserver(observer1);
		istorage.addObserver(observer2);
		istorage.addObserver(observer3);

		istorage.setValue(5);
		istorage.setValue(2);
		istorage.setValue(25);
		istorage.setValue(13);
		istorage.setValue(22);
		istorage.setValue(15);
		istorage.setValue(48);

	}

}
