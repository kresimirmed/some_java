package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This is example for observations.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ObserverExample {

	/**
	 * This is main method, what contains example.
	 * 
	 * @param args
	 *            Unused
	 */
	public static void main(String[] args) {

		IntegerStorage istorage = new IntegerStorage(20);
		IntegerStorageObserver observer = new SquareValue();

		istorage.addObserver(observer);

		istorage.setValue(5);
		istorage.setValue(2);
		istorage.setValue(25);

		istorage.removeObserver(observer);

		istorage.addObserver(new ChangeCounter());
		istorage.addObserver(new DoubleValue(2));
		istorage.addObserver(new DoubleValue(2));
		istorage.addObserver(new DoubleValue(1));
		istorage.addObserver(new DoubleValue(2));

		istorage.setValue(13);
		istorage.setValue(22);
		istorage.setValue(15);
		istorage.setValue(48);

	}

}
