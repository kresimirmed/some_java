package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class write a square of the integer stored in the {@code IntegerStorage}
 * to the standard output.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class SquareValue implements IntegerStorageObserver {

	@Override
	public void valueChanged(IntegerStorageChange intStorageChange) {
		System.out.println("Provided new value: " + intStorageChange.getNewValue() + ", squere is "
				+ Math.pow((double) intStorageChange.getNewValue(), 2));
	}

}
