package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.Optional;

/**
 * This serves to demonstrate {@code LikeMedian}. This is first demonstration.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class MedianDemo1 {

	/**
	 * This is main method it start when somebody call this class.
	 * 
	 * @param args
	 *            Unused.
	 */
	public static void main(String[] args) {

		LikeMedian<Integer> likeMedian = new LikeMedian<Integer>();
		likeMedian.add(new Integer(10));
		likeMedian.add(new Integer(3));
		likeMedian.add(new Integer(8));
		likeMedian.add(new Integer(7));
		likeMedian.add(new Integer(5));
		likeMedian.add(new Integer(1));
		likeMedian.add(new Integer(2));
		Optional<Integer> result = likeMedian.get();

		System.out.println(result.get());
		for (Integer elem : likeMedian) {
			System.out.println(elem);
		}

	}

}
