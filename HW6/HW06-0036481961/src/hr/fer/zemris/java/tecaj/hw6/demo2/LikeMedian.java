package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This class have one list where save elements type of {@code T}. Whit method
 * {@code get} return median of list.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 * @param <T>
 *            The type of elements returned by the iterator and element of
 *            class.
 */
public class LikeMedian<T extends Object> implements Iterable<T> {

	/**
	 * Represent list of elements <T>.
	 */
	private List<T> list;

	/**
	 * Default constructor.
	 */
	public LikeMedian() {
		list = new ArrayList<>();
	}

	/**
	 * Method if user provided even number of elements, method must return the
	 * smaller from the two elements which would usually be used to calculate
	 * median element. If user provided adds number of elements, method must
	 * return median element.
	 * 
	 * @return Median element.
	 */
	public Optional<T> get() {
		int size = list.size();
		List<T> newList = list.stream().sorted().collect(Collectors.toList());
		if (size == 0) {
			return Optional.empty();
		} else if (size % 2 == 0) {
			return Optional.of(newList.get(size / 2 - 1));
		} else {
			return Optional.of(newList.get(size / 2));
		}
	}

	/**
	 * Appends the specified element to the end of this list.
	 * 
	 * @param value
	 *            element to be appended to this list.
	 */
	public void add(T value) {
		list.add(value);
	}

	/**
	 * Returns an iterator over elements of type {@code T}.
	 */
	public Iterator<T> iterator() {
		return new MyIterator();
	}

	/**
	 * This class implements iterator over elements of type{@code T} for
	 * {@code LikeMedian} class.
	 * 
	 * @author Krešimir Medvidović
	 * @version 1.0
	 *
	 */
	private class MyIterator implements Iterator<T> {

		/**
		 * Represent iterator of {@code list}
		 */
		Iterator<T> iter = list.iterator();

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return iter.hasNext();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public T next() {
			return iter.next();
		}

	}
}
