package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.Optional;

/**
 * This serves to demonstrate {@code LikeMedian}. This is second demonstration.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class MedianDemo2 {

	/**
	 * This is main method it start when somebody call this class.
	 * 
	 * @param args
	 *            Unused.
	 */
	public static void main(String[] args) {

		LikeMedian<String> likeMedian = new LikeMedian<String>();
		likeMedian.add("Joe");
		likeMedian.add("Jane");
		likeMedian.add("Adam");
		likeMedian.add("Zed");
		Optional<String> result = likeMedian.get();
		System.out.println(result.get()); // Writes: Jane

	}

}
