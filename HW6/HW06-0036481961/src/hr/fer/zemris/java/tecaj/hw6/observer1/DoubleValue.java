package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * DoubleValue class write to the standard output double value of the current
 * value which is stored in {@code IntegerStorage}, but only first n times since
 * its registration with subject (n is given in constructor). After writing the
 * double value form the n-th time, the observer automatically de-registers
 * itself from the {@code IntegerStorage}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Represent how many times will double value which is stored in subject.
	 */
	private int n;

	/**
	 * Constructor receiving how many times will double and print value which is
	 * stored in subject.
	 * 
	 * @param n
	 *            Number of times.
	 */
	public DoubleValue(int n) {
		this.n = n;
	}

	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Double value: " + (istorage.getValue() * 2));

		--n;
		if (n == 0) {
			istorage.removeObserver(this);
		}
	}

}
