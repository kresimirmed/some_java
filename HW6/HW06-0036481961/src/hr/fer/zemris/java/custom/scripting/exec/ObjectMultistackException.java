package hr.fer.zemris.java.custom.scripting.exec;

/**
 * {@code ObjectMultistackException} is exception made just for
 * {@code ObjectMultistack} class.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ObjectMultistackException extends RuntimeException {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new ObjectMultistack exception with {@code null} as its
	 * detail message.
	 */
	public ObjectMultistackException() {
	}

	/**
	 * Constructs a new ObjectMultistack exception with the specified detail
	 * message.
	 * 
	 * @param massage
	 *            The detail message.
	 */
	public ObjectMultistackException(String massage) {
		super(massage);
	}

	/**
	 * Constructs a new ObjectMultistack exception with the specified cause and
	 * a detail message of (cause==null ? null : cause.toString())
	 * 
	 * @param cause
	 *            The cause.
	 */
	public ObjectMultistackException(Throwable cause) {
		super(cause);
	}

	/**
	 * onstructs a new ObjectMultistack exception with the specified detail
	 * message and cause.
	 * 
	 * @param massage
	 *            The detail message.
	 * @param cause
	 *            The cause.
	 */
	public ObjectMultistackException(Throwable cause, String massage) {
		super(massage, cause);
	}

}
