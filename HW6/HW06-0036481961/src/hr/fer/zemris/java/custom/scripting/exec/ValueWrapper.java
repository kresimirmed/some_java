package hr.fer.zemris.java.custom.scripting.exec;

/**
 * Class {@link ValueWrapper} represent value what is associated with key in
 * {@link ObjectMultistack}. It have a read-write property {@code value} of type
 * Object, a single public constructor that accepts initial value and prescribed
 * methods.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ValueWrapper {

	/**
	 * Read-write value of type object.
	 */
	private Object value;

	/**
	 * Constructor witch receive value of our object.
	 * 
	 * @param value
	 *            Initial value.
	 */
	public ValueWrapper(Object value) {
		if (value == null) {
			this.value = 0;
		} else {
			this.value = convertToNumber(value);
		}
	}

	/**
	 * Getter for value.
	 * 
	 * @return value.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Stter for {@code value}.
	 * 
	 * @param value
	 *            New value.
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * Method increment {@code value} with {@code incValue}.
	 * 
	 * @param incValue
	 *            Augend.
	 */
	public void increment(Object incValue) {
		if (incValue == null) {
			incValue = 0;
		} else if (incValue instanceof String) {
			incValue = convertToNumber(incValue);
		}
		if (value instanceof Integer && incValue instanceof Integer) {
			value = (Integer) value + (Integer) incValue;
		} else if (value instanceof Double && incValue instanceof Double) {
			value = (double) value + (double) incValue;
		} else if (value instanceof Integer && incValue instanceof Double) {
			value = (Integer) value + (double) incValue;
		} else {
			value = (double) value + (Integer) incValue;
		}
	}

	/**
	 * Method decreases {@code value} for {@code decValue}.
	 * 
	 * @param decValue
	 *            Subtrahend.
	 */
	public void decrement(Object decValue) {
		if (decValue == null) {
			decValue = 0;
		} else if (decValue instanceof String) {
			decValue = convertToNumber(decValue);
		}
		if (value instanceof Integer && decValue instanceof Integer) {
			value = (Integer) value - (Integer) decValue;
		} else if (value instanceof Double && decValue instanceof Double) {
			value = (double) value - (double) decValue;
		} else if (value instanceof Integer && decValue instanceof Double) {
			value = (Integer) value - (double) decValue;
		} else {
			value = (double) value - (Integer) decValue;
		}
	}

	/**
	 * Method multiply {@code value} with {@code mulValue}.
	 * 
	 * @param mulValue
	 *            Multiplier.
	 */
	public void multiply(Object mulValue) {
		if (mulValue == null) {
			mulValue = 0;
		} else if (mulValue instanceof String) {
			mulValue = convertToNumber(mulValue);
		}
		if (value instanceof Integer && mulValue instanceof Integer) {
			value = (Integer) value * (Integer) mulValue;
		} else if (value instanceof Double && mulValue instanceof Double) {
			value = (double) value * (double) mulValue;
		} else if (value instanceof Integer && mulValue instanceof Double) {
			value = (Integer) value * (double) mulValue;
		} else {
			value = (double) value * (Integer) mulValue;
		}
	}

	/**
	 * Method divide {@code value} with {@code divValue}. If both number are
	 * type of Integer and module this number is not equal to zero, method
	 * convert result to {@code Double}, it divide like we have two
	 * {@code Double} type. If users try divide {@code value} with {@code 0}
	 * method throw {@link IllegalArgumentException}.
	 * 
	 * @param divValue
	 *            Divisor.
	 */
	public void divide(Object divValue) {
		if (divValue == null) {
			divValue = 0;
		} else if (divValue instanceof String) {
			divValue = convertToNumber(divValue);
		}
		// One for integer and one for double type
		if (divValue.equals(0) || divValue.equals(0.0)) {
			throw new IllegalArgumentException("You can not devide with zero!");
		}
		if (value instanceof Integer && divValue instanceof Integer) {
			if ((Integer) value % (Integer) divValue == 0) {
				value = (Integer) value / (Integer) divValue;
			} else {
				value = (Integer) value / (double) (Integer) divValue;
			}
		} else if (value instanceof Double && divValue instanceof Double) {
			value = (double) value / (double) divValue;
		} else if (value instanceof Integer && divValue instanceof Double) {
			value = (Integer) value / (double) divValue;
		} else {
			value = (double) value / (Integer) divValue;
		}
	}

	/**
	 * Compare {@code value} with received object {@code withValue}, before
	 * comparison method see this object like String. The result is a negative
	 * integer if this {@code String} object lexicographically precedes the
	 * argument string. The result is a positive integer if this {@code String}
	 * object lexicographically follows the argument string. The result is zero
	 * if the strings are equal; {@code compareTo} returns {@code 0} exactly
	 * when the {@link #equals(Object)} method would return {@code true}.
	 * 
	 * @param withValue
	 *            The {@code Object} to be compared.
	 * @return Integer value.
	 */
	public int numCompare(Object withValue) {
		return value.toString().compareTo(withValue.toString());
	}

	/**
	 * Check is received object can convert in Integer type.
	 * 
	 * @param str
	 *            Received object.
	 * @return True if we can parse it in Integer, otherwise false.
	 */
	private static boolean isInteger(Object str) {
		try {
			Integer.parseInt(str.toString());
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;
	}

	/**
	 * Check is received object can convert in Double type.
	 * 
	 * @param str
	 *            Received object.
	 * @return True if we can parse it in Double, otherwise false
	 */
	private static boolean isDouble(Object str) {
		try {
			Double.parseDouble(str.toString());
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;
	}

	/**
	 * Parse recived object{@code str} to Integer type.
	 * 
	 * @param str
	 *            Recived object.
	 * @return Integer value.
	 */
	private int parsInt(Object str) {
		return Integer.parseInt(str.toString());
	}

	/**
	 * Parse received object {@code str} to Double type.
	 * 
	 * @param str
	 *            Received object.
	 * @return Double value.
	 */
	private double parsDouble(Object str) {
		return Double.parseDouble(str.toString());
	}

	/**
	 * This method converted recived object {@code value} in to number.Number is
	 * type of Double or Integer.
	 * 
	 * @param value
	 *            Received object.
	 * @return new number.
	 */
	private Object convertToNumber(Object value) {
		if (isInteger(value)) {
			value = parsInt(value.toString());
		} else if (isDouble(value)) {
			value = parsDouble(value.toString());
		} else {
			throw new RuntimeException("This is not number!");
		}
		return value;
	}

}
