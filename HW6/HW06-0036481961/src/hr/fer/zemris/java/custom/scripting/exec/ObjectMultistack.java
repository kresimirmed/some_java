package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represent multi-stack, it is something like a Map but a special
 * kind. While Map allows you only to store for each key a single value, this
 * class must allow the user to store multiple values for same key and it must
 * provide stack. Key for {@code ObjectMultistack} will be instance of class
 * String, value that will be associated with those keys will be instances of
 * {@code ValueWrapper}.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ObjectMultistack {

	/**
	 * Represent our multistack.
	 */
	public static Map<String, MultistackEntry> multiStack;

	/**
	 * Default constructor.
	 */
	public ObjectMultistack() {
		multiStack = new HashMap<>();
	}

	/**
	 * pushes given value on the stack. null value must not be allowed to be
	 * placed on stack.
	 * 
	 * @param name
	 *            Stack name.
	 * @param valueWrapper
	 *            Value what will be push on stack {@code name}.
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		if (multiStack.containsKey(name)) {
			MultistackEntry node = multiStack.get(name);
			multiStack.put(name, node.addNode(valueWrapper));
		} else {
			multiStack.put(name, new MultistackEntry(valueWrapper, null));
		}
	}

	/**
	 * Removes last value pushed on stack from stack and returns it. If the
	 * stack is empty when method pop is called, the method should throw
	 * Exception. Return {@code null} if this map contains no mapping for the
	 * key.
	 * 
	 * @param name
	 *            Stack name.
	 * @return Last element on stack.
	 */
	public ValueWrapper pop(String name) {
		ValueWrapper returnValue = peek(name);
		MultistackEntry node = multiStack.get(name);
		multiStack.put(name, node.removeNode());
		return returnValue;
	}

	/**
	 * Similar as pop; returns last element placed on stack but does not delete
	 * it from stack. Handle an empty stack as described in pop method.
	 * 
	 * @param name
	 *            Stack name.
	 * @return Last element on stack.
	 */
	public ValueWrapper peek(String name) {
		if(multiStack.get(name)==null){
			throw new ObjectMultistackException("Stack is empty!");
		}
		return multiStack.get(name).getObject();
	}

	/**
	 * Returns true if collection contains no objects and false otherwise.
	 * 
	 * @param name
	 *            Stack name.
	 * @return true if stack is empty, otherwise false.
	 */
	public boolean isEmpty(String name) {
		if (multiStack.containsKey(name)) {
			if (multiStack.get(name) == null) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Inner static class that acts as a node of a single.linked list.
	 * 
	 * @author Krešimir Medvidović
	 * @version 1.0
	 *
	 */
	private static class MultistackEntry {

		/**
		 * Value of node.
		 */
		private ValueWrapper object;

		/**
		 * Next node in list.
		 */
		private MultistackEntry next;

		/**
		 * Constructor witch received value of node and reference to next
		 * {@code ValueWrapper}.
		 * 
		 * @param object
		 *            Value of node.
		 * @param next
		 *            Reference to next node.
		 */
		public MultistackEntry(ValueWrapper object, MultistackEntry next) {
			this.object = object;
			this.next = next;
		}

		/**
		 * Getter for object in node.
		 * 
		 * @return Object stored in node.
		 */
		public ValueWrapper getObject() {
			return object;
		}

		/**
		 * Add at first position in single-linked list.
		 * 
		 * @param object
		 *            Value of list node.
		 * @return New single-linked list.
		 */
		public MultistackEntry addNode(ValueWrapper object) {
			return new MultistackEntry(object, this);
		}

		/**
		 * Remove first element in single-linked list.
		 * 
		 * @return New single-linked list.
		 */
		public MultistackEntry removeNode() {
			return this.next;
		}
	}
}
