package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

@SuppressWarnings("javadoc")
public class MultistackTestes {

	@SuppressWarnings("unused")
	@Test(expected = RuntimeException.class)
	public void testRuntime() {
		ValueWrapper year = new ValueWrapper(String.valueOf("123fi"));
	}

	@Test(expected = RuntimeException.class)
	public void testRuntime2() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("year", year);
		multistack.peek("year").setValue(((Integer) multistack.peek("year").getValue()).intValue() + 50);
		multistack.peek("year").increment("2e");
	}

	@Test
	public void testSet() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2050);
	}

	@Test
	public void testIncrement() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").increment(50);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2100);
	}

	@Test
	public void testIncrement2() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").increment(50.4);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2100.4);
	}

	@Test
	public void testIncrement3() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50.54);
		multistack.peek("kreso").increment(50.4);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2100.94);
	}

	@Test
	public void testIncrement4() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50.514);
		multistack.peek("kreso").increment(50);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2100.514);
	}

	@Test
	public void testDecrement() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").decrement(50.4);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 1999.6);
	}

	@Test
	public void testDecrement2() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50.5);
		multistack.peek("kreso").decrement(50.4);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2000.1);
	}

	@Test
	public void testDecrement3() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 510.195);
		multistack.peek("kreso").decrement(50);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2460.195);
	}

	@Test
	public void testDecrement4() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").decrement(100);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 1950);
	}

	@Test
	public void testMul() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").multiply(2);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 4100);
	}

	@Test
	public void testMul2() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() + 50);
		multistack.peek("kreso").multiply(2.2);

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 2050 * 2.2);
	}

	@Test
	public void testMul3() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 84);
		multistack.peek("kreso").multiply("2.58");

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), (2000 - 84) * 2.58);
	}

	@Test
	public void testMul4() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 84);
		multistack.peek("kreso").multiply("2.58");

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), (1950 - 84) * 2.58);
	}

	@Test
	public void testMul5() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 84);
		multistack.peek("kreso").multiply("2.58");
		multistack.peek("kreso").multiply("1950");

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), (2000 - 84) * 2.58 * 1950);
	}

	@Test
	public void testMul6() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");

		assertEquals("Must be same!", multistack.peek("kreso").getValue(), 1950);
	}

	@Test
	public void testPush() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950);
	}

	@Test(expected = ObjectMultistackException.class)
	public void testPeekEx() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(null);
		year.increment("1950");
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950);
		multistack.peek("kreso");

	}

	@Test(expected = ObjectMultistackException.class)
	public void testPopEx() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950);
		multistack.pop("kreso");

	}

	@Test
	public void testDiv() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);
		multistack.peek("kreso").divide("2.2");

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2 / 2.2);

		multistack.peek("kreso").divide("15");
		multistack.peek("kreso").decrement(-50);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950 / 15 + 50);

	}

	@Test
	public void testDiv2() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);
		multistack.peek("kreso").divide("2.2");

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2 / 2.2);

		multistack.peek("kreso").divide("15");
		multistack.peek("kreso").decrement(-50);
		multistack.peek("kreso").divide(1548.4568);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), (1950 / 15 + 50) / 1548.4568);

	}

	@Test
	public void testNull() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").decrement(null);
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").increment(null);
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(null);

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), (double) 0);

		multistack.peek("kreso").divide("15");
		multistack.peek("kreso").decrement(-50);
		multistack.peek("kreso").divide(1548.4568);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), (1950 / 15 + 50) / 1548.4568);

	}

	@Test
	public void testCompare() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		multistack.push("kreso", year);
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2");
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").multiply("1950");
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);
		multistack.peek("kreso").divide("2.2");

		assertEquals("Must be same!", multistack.peek("kreso").numCompare(1950.1950 * 2 / 1.01) > 0, false);
		assertEquals("Must be same!", multistack.pop("kreso").numCompare(1950.1950 * 2 / 0.99) < 0, true);

		multistack.peek("kreso").divide("15");
		multistack.peek("kreso").decrement(-50);
		assertEquals(multistack.isEmpty("kreso"), false);
		assertEquals("Must be same!", multistack.peek("kreso").numCompare(1950 / 15 + 50.0001) > 0, false);
		assertEquals("Must be same!", multistack.peek("kreso").numCompare(1950 / 15 + 49.99999) > 0, true);
		assertEquals("Must be same!", multistack.pop("kreso").numCompare(1950 / 15 + 50), 0);
		assertEquals(multistack.isEmpty("kreso"), true);
		assertEquals(multistack.isEmpty("kreso"), true);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testDiv0() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("year", year);
		multistack.peek("year").setValue(((Integer) multistack.peek("year").getValue()).intValue() + 50);
		multistack.peek("year").divide(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDiv02() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("year", year);
		multistack.peek("year").divide(new Integer(3));
		multistack.peek("year").divide(new Integer(8));
		multistack.peek("year").divide("0");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDiv03() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("year", year);
		multistack.peek("year").setValue(((Integer) multistack.peek("year").getValue()).intValue() + 50);
		multistack.peek("year").divide(0.0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDiv04() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf(2000));
		multistack.push("year", year);
		multistack.peek("year").setValue(((Integer) multistack.peek("year").getValue()).intValue() + 50);
		multistack.peek("year").divide(null);
	}
	
	@Test
	public void testAll() {
		ObjectMultistack multistack = new ObjectMultistack();
		ValueWrapper year = new ValueWrapper(Integer.valueOf("1950"));
		
		multistack.push("kreso", year);
		
		multistack.peek("kreso").setValue(((Integer) multistack.peek("kreso").getValue()).intValue() - 1950);
		multistack.peek("kreso").multiply("2.2");
		multistack.peek("kreso").multiply("1.1");
		multistack.peek("kreso").divide(new Double(2.42));
		multistack.peek("kreso").decrement("-2");
		multistack.peek("kreso").increment("-1");
		multistack.peek("kreso").decrement(null);
		multistack.peek("kreso").increment("0");
		multistack.peek("kreso").divide(-2.25);
		multistack.peek("kreso").multiply("1950");
		multistack.peek("kreso").multiply(-2.25);
		
		multistack.push("kreso", new ValueWrapper(new Double(1950.1950)));
		multistack.peek("kreso").multiply(2);
		multistack.peek("kreso").divide("2.2");
		
		multistack.push("kreso", new ValueWrapper(null));
		multistack.peek("kreso").increment(1.0);
		multistack.peek("kreso").decrement(-1);
		multistack.peek("kreso").divide(2);
		multistack.peek("kreso").multiply(1995.1950);
		
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1995.1950);
		
		multistack.peek("kreso").divide(5.5);
		multistack.peek("kreso").multiply(-11);

		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1950.1950 * 2 / 2.2/5.5*(-11));
		
		multistack.push("kreso", new ValueWrapper(null));
		multistack.peek("kreso").increment(1.0);
		multistack.peek("kreso").decrement(-1);
		multistack.peek("kreso").divide(2);
		multistack.peek("kreso").multiply(1995.1950);
		
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), 1995.1950);

		multistack.peek("kreso").divide("15");
		multistack.peek("kreso").decrement(-50);
		multistack.peek("kreso").divide(1548.4568);
		assertEquals("Must be same!", multistack.pop("kreso").getValue(), (1950 / 15 + 50) / 1548.4568);

	}

}
