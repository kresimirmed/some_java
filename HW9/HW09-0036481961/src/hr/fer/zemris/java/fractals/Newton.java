package hr.fer.zemris.java.fractals;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.complex.*;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;

/**
 * This class ask from user to input few root and with received root call other
 * method for calculating and printing specific fractal.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Newton {

	/**
	 * List of complex number what user enter over keyboard.
	 */
	private static List<Complex> list;

	/**
	 * This is main method and it start with this class. Her job is to call
	 * other method and print fractal.
	 * 
	 * @param args
	 *            Unused
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.\n"
				+ "Please enter at least two roots, one root per line. Enter \'done\' when done.");
		list = new ArrayList<>();

		readInput();

		ComplexRootedPolynomial cRootedPol = new ComplexRootedPolynomial(list.toArray(new Complex[0]));

		FractalViewer.show(new FractalProducer(cRootedPol));

	}

	/**
	 * This method read entered text, call {@code parse} and put complex number
	 * in {@code list}.
	 */
	private static void readInput() {

		BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));

		int i = 1;

		while (true) {
			String line;
			try {
				System.out.printf("Root " + i + "> ");
				line = reader.readLine().trim();
				if (line.isEmpty()) {
					System.err.println("Line is empty!");
					continue;
				}
			} catch (IOException e) {
				System.err.println("Something happen with reader, so try again!");
				continue;
			}
			if (line.equals("done")) {
				break;
			} else {
				try {
					Complex number = parse(line);
					list.add(number);
					++i;
				} catch (NumberFormatException e) {
					System.err.println("Wrong arguments given!\n");
					continue;
				}

			}
		}
	}

	/**
	 * This method parse received {@code String}
	 * 
	 * @param in
	 *            Read {@code String}
	 * @return Complex number what been in {@code in}
	 */
	private static Complex parse(String in) {
		String[] parts = in.split("\\s*[+-]\\s*");
		int re = 0, im = 0, pos = -1;
		for (String s : parts) {
			if (pos != -1) {
				s = in.charAt(pos) + s;
			} else {
				pos = 0;
				if ("".equals(s)) {
					continue;
				}
			}
			pos += s.length();
			if (s.lastIndexOf('i') == -1) {
				if (!"+".equals(s) && !"-".equals(s)) {
					re += Integer.parseInt(s.trim());
				}
			} else {
				s = s.replace("i", "");
				s = s.replace(" ", "");
				if ("+".equals(s)) {
					im++;
				} else if ("-".equals(s)) {
					im--;
				} else {
					if (s.trim().isEmpty()) {
						im += Integer.parseInt("1");
					} else {
						im += Integer.parseInt(s.trim());
					}
				}
			}
		}
		return new Complex(re, im);
	}

}
