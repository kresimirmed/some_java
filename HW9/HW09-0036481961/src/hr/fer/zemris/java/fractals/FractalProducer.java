package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import hr.fer.zemris.java.complex.*;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

/**
 * This class implement {@code IFractalProducer} and her job is to reduce
 * fractal on monitor. We in this class use multy-threaded.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class FractalProducer implements IFractalProducer {

	/**
	 * Represent convergence treshold.
	 */
	private static final double CONVERGENCE_TRESHOLD = 1e-3;

	/**
	 * Represent root treshold.
	 */
	private static final double ROOT_TRESHOLD = 2e-3;

	/**
	 * Represent number of iteration.
	 */
	private static final int MAX_ITER = 10_000;

	/**
	 * Represent {@code ComplexRootedPolynomial}, we recive this from main
	 * method.
	 */
	private ComplexRootedPolynomial cRootedPol;

	/**
	 * Represent upper bound for examination of iterations.
	 */
	private static short m;

	/**
	 * Constructor witch receive {@code ComplexRootedPolynomial} and initial
	 * this {@code m} and {@code cRootedPol}
	 * 
	 * @param cRootedPol
	 *            Array of {@code Complex} number for
	 *            {@code ComplexRootedPolynomial}.
	 */
	public FractalProducer(ComplexRootedPolynomial cRootedPol) {
		this.cRootedPol = cRootedPol;
		m = (short) (cRootedPol.toComplexPolynom().order() + 1);
	}

	@Override
	public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
			IFractalResultObserver observer) {

		short[] data = new short[height * width];

		class FractalJob implements Callable<Void> {
			/**
			 * y point on screen where thread start job.
			 */
			int yMin;
			/**
			 * y point on screen where thread end job.
			 */
			int yMax;

			/**
			 * Constructor for this job.
			 * 
			 * @param yMin
			 *            Starting point.
			 * @param yMax
			 *            Finish point.
			 */
			public FractalJob(int yMin, int yMax) {
				super();
				this.yMin = yMin;
				this.yMax = yMax;
			}

			@Override
			public Void call() throws Exception {
				calculate(reMin, reMax, imMin, imMax, width, height, m, yMin, yMax, data);
				return null;
			}

		}

		final int iCore = 8 * Runtime.getRuntime().availableProcessors();

		int heightPart = height / iCore;
		ExecutorService pool = Executors.newFixedThreadPool(iCore);
		List<Future<Void>> results = new ArrayList<>();

		for (int i = 0; i < iCore; i++) {
			int yMin = i * heightPart;
			int yMax = (i + 1) * heightPart - 1;
			if (i == iCore - 1) {
				yMax = height - 1;
			}

			FractalJob job = new FractalJob(yMin, yMax);

			results.add(pool.submit(job));
		}

		for (Future<Void> job : results) {
			try {
				job.get();
			} catch (InterruptedException | ExecutionException ignorable) {
			}
		}
		pool.shutdown();

		observer.acceptResult(data, m, requestNo);
	}

	/**
	 * This method calculate fractal for given {@code ComplexRootedPolynomial}.
	 * 
	 * @param reMin
	 *            Minimum of real part of complex number
	 * @param reMax
	 *            Maximum of real part of complex number
	 * @param imMin
	 *            Minimum of imaginary part of complex number
	 * @param imMax
	 *            Maximum of imaginary part of complex number
	 * @param width
	 *            Width of raster for visualization of fractal.
	 * @param height
	 *            Height of raster for visualization of fractal.
	 * @param m
	 *            Upper bound for examination of iterations.
	 * @param ymin
	 *            First point of thread job.
	 * @param ymax
	 *            Last point of thread job.
	 * @param data
	 *            Holds the indexes of roots in which observed complex point c
	 *            has converged or 0 if no convergence to a root occurred.
	 */
	private void calculate(double reMin, double reMax, double imMin, double imMax, int width, int height, int m,
			int ymin, int ymax, short[] data) {
		int offset = ymin * height;

		for (int y = ymin; y <= ymax; y++) {
			for (int x = 0; x < width; x++) {
				double cre = x * (reMax - reMin) / (width - 1) + reMin;
				double cim = (height - 1 - y) * (imMax - imMin) / (height - 1) + imMin;
				Complex c = new Complex(cre, cim);
				Complex zn = c;
				Complex zn1;
				int iter = 0;
				double module;
				do {
					Complex numerator = cRootedPol.apply(zn);
					Complex denominator = cRootedPol.toComplexPolynom().derive().apply(zn);
					Complex fraction = numerator.divide(denominator);
					zn1 = zn.sub(fraction);
					module = zn1.sub(zn).module();
					iter++;
					zn = zn1;
				} while (module > CONVERGENCE_TRESHOLD && iter < MAX_ITER);
				int index = cRootedPol.indexOfClosestRootFor(zn1, ROOT_TRESHOLD);
				data[offset++] = (short) ((index == -1) ? 1 : index + 1);
			}
		}
	}

}
