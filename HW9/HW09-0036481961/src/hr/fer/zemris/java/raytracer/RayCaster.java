package hr.fer.zemris.java.raytracer;

import java.util.List;

import hr.fer.zemris.java.raytracer.model.*;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * This class show 3D picture on screen.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class RayCaster {

	/**
	 * Add for hole in picture
	 */
	public final static double PLUS_DISTANCE = 0.0000001;

	/**
	 * Main method start with this class.
	 * 
	 * @param args
	 *            Unused.
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}

	/**
	 * Method get {@code IRayTracerProducer}
	 * 
	 * @return IRayTracerProducer.
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer) {
				System.out.println("Započinjem izračune...");
				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D zAxis = view.sub(eye).modifyNormalize();
				Point3D yAxis = viewUp.normalize().sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp.normalize())));
				Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();
				Point3D screenCorner = view.sub(xAxis.scalarMultiply(horizontal / 2.0))
						.add(yAxis.scalarMultiply(vertical / 2.0));

				Scene scene = RayTracerViewer.createPredefinedScene();
				short[] rgb = new short[3];
				int offset = 0;
				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {

						Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply(x / (width - 1.0) * horizontal))
								.sub(yAxis.scalarMultiply(y / (height - 1.0) * vertical));

						Ray ray = Ray.fromPoints(eye, screenPoint);

						tracer(scene, ray, rgb);

						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
						offset++;
					}
				}
				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}

			/**
			 * Method trace ray from eye to someone point on scene. If ray do
			 * not hit any shape on scene rgb is equal to (0,0,0).
			 * 
			 * @param scene
			 *            Scene on screen.
			 * @param ray
			 *            Ray from eye to screenPoint.
			 * @param rgb
			 *            Array of red, green and blue color.
			 */
			private void tracer(Scene scene, Ray ray, short[] rgb) {

				RayIntersection closestPoint = findClosest(ray, scene);
				if (closestPoint == null) {
					for (int i = 0; i < rgb.length; ++i) {
						rgb[i] = 0;
					}
				} else {
					double[] calcRGB = new double[] { 15, 15, 15 };
					calculateRGB(scene, ray, closestPoint, calcRGB);
					rgb[0] = (short) calcRGB[0];
					rgb[1] = (short) calcRGB[1];
					rgb[2] = (short) calcRGB[2];
				}

			}

			/**
			 * Method finde closest ray intersection for received {@code ray}
			 * and {@code scene}.
			 * 
			 * @param ray
			 *            Ray
			 * @param scene
			 *            Scene
			 * @return Closest ray intersection.
			 */
			private RayIntersection findClosest(Ray ray, Scene scene) {
				RayIntersection closestPoint = null;
				List<GraphicalObject> list = scene.getObjects();

				for (GraphicalObject element : list) {
					RayIntersection help = element.findClosestRayIntersection(ray);
					if (help == null) {
						continue;
					} else if (closestPoint == null || (closestPoint.getDistance() > help.getDistance())) {
						closestPoint = help;
					}
				}
				return closestPoint;
			}

			/**
			 * Method calculate color component for objects.
			 * 
			 * @param scene
			 *            Scene with objects.
			 * @param ray
			 *            Ray from observer to someone point
			 * @param intersection
			 *            Point where ray hit object on scene.
			 * @param rgb
			 *            Tank for color intensity in some point.
			 */
			private void calculateRGB(Scene scene, Ray ray, RayIntersection intersection, double[] rgb) {
				List<LightSource> list = scene.getLights();

				for (LightSource lightSource : list) {
					Ray light = Ray.fromPoints(lightSource.getPoint(), intersection.getPoint());
					RayIntersection forAnyObject = findClosest(light, scene);

					if (forAnyObject != null) {
						double lightSourceDistance = lightSource.getPoint().sub(forAnyObject.getPoint()).norm();
						double eyeDistance = lightSource.getPoint().sub(intersection.getPoint()).norm();

						if (Double.compare(lightSourceDistance + PLUS_DISTANCE, eyeDistance) >= 0) {
							diffuseComponent(lightSource, forAnyObject, rgb);
							reflectiveComponent(lightSource, ray, forAnyObject, rgb);

						}
					}
				}
			}

			/**
			 * Method calculate reflective component for color.
			 * 
			 * @param lightSource
			 *            Source of light for ray on sphere.
			 * @param intersection
			 *            Ray witch hit sphere.
			 * @param rgb
			 *            Tank for color intensity in some point.
			 */
			private void reflectiveComponent(LightSource lightSource, Ray ray, RayIntersection intersection,
					double[] rgb) {
				Point3D projection = intersection.getNormal().scalarMultiply(
						lightSource.getPoint().sub(intersection.getPoint()).scalarProduct(intersection.getNormal()));
				Point3D r = projection
						.add(projection.negate().add(lightSource.getPoint().sub(intersection.getPoint())).negate());
				double cos = r.normalize().scalarProduct(ray.start.sub(intersection.getPoint()).normalize());

				if (Double.compare(cos, 0) >= 0) {
					double n = Math.pow(cos, intersection.getKrn());
					rgb[0] += lightSource.getR() * intersection.getKrr() * n;
					rgb[1] += lightSource.getG() * intersection.getKrg() * n;
					rgb[2] += lightSource.getB() * intersection.getKrb() * n;
				}

			}

			/**
			 * Method calculate diffuse component for color.
			 * 
			 * @param lightSource
			 *            Source of light for ray on sphere.
			 * @param intersection
			 *            Ray witch hit sphere.
			 * @param rgb
			 *            Tank for color intensity in some point.
			 */
			private void diffuseComponent(LightSource lightSource, RayIntersection intersection, double[] rgb) {
				Point3D point = lightSource.getPoint().sub(intersection.getPoint()).normalize();
				double n = point.scalarProduct(intersection.getNormal());

				if (n > 0) {
					rgb[0] += lightSource.getR() * intersection.getKdr() * n;
					rgb[1] += lightSource.getG() * intersection.getKdg() * n;
					rgb[2] += lightSource.getB() * intersection.getKdb() * n;
				}
			}
		};
	}

}
