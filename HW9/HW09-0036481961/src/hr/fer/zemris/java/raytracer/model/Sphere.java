package hr.fer.zemris.java.raytracer.model;

/**
 * This class implement {@code GraphicalObject}. It represent a sphere.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Sphere extends GraphicalObject {

	/**
	 * Center of sphere.
	 */
	private Point3D center;

	/**
	 * Radius of sphere.
	 */
	private double radius;

	/**
	 * Red diffuse component.
	 */
	private double kdr;

	/**
	 * Green diffuse component.
	 */
	private double kdg;

	/**
	 * Blue diffuse component.
	 */
	private double kdb;

	/**
	 * Red reflective component.
	 */
	private double krr;

	/**
	 * Green reflective component.
	 */
	private double krg;

	/**
	 * Blue reflective component.
	 */
	private double krb;

	/**
	 * Coefficient n.
	 */
	private double krn;

	/**
	 * This is constructor for sphere.
	 * 
	 * @param center
	 *            Center of sphere.
	 * @param radius
	 *            Radius of sphere.
	 * @param kdr
	 *            Red diffuse component.
	 * @param kdg
	 *            Green diffuse component.
	 * @param kdb
	 *            Blue diffuse component.
	 * @param krr
	 *            Red reflective component.
	 * @param krg
	 *            Green reflective component.
	 * @param krb
	 *            Blue reflective component.
	 * @param krn
	 *            Coefficient n.
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg, double kdb, double krr, double krg, double krb,
			double krn) {
		super();
		this.center = center;
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}

	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		Point3D l = ray.direction;
		Point3D o = ray.start;
		double underSqrt = Math.pow(l.scalarProduct(o.sub(center)), 2) - Math.pow((o.sub(center)).norm(), 2)
				+ Math.pow(radius, 2);
		double d;
		if (underSqrt < 0) {
			return null;
		} else if (underSqrt == 0) {
			d = -l.scalarProduct(o.sub(center));
		} else {
			double outSqrt = -l.scalarProduct(o.sub(center));
			d = Math.min(outSqrt + Math.sqrt(underSqrt), outSqrt - Math.sqrt(underSqrt));
		}
		Point3D point = o.add(l.scalarMultiply(d));
		return new RayIntersection(point, d, false) {

			@Override
			public Point3D getNormal() {
				return super.getPoint().sub(center).normalize();
			}

			@Override
			public double getKdr() {
				return kdr;
			}

			@Override
			public double getKdg() {
				return kdg;
			}

			@Override
			public double getKdb() {
				return kdb;
			}

			@Override
			public double getKrr() {
				return krr;
			}

			@Override
			public double getKrg() {
				return krg;
			}

			@Override
			public double getKrb() {
				return krb;
			}

			@Override
			public double getKrn() {
				return krn;
			}

		};
	}

}
