package hr.fer.zemris.java.complex;

/**
 * This class represent Complex polynom with his operations.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ComplexPolynomial {

	/**
	 * Factors of this complex polynomial.
	 */
	private Complex[] factors;

	/**
	 * Constructor that receive factors.
	 * 
	 * @param factors
	 *            Factors of complex polynomial.
	 */
	public ComplexPolynomial(Complex... factors) {
		this.factors = new Complex[factors.length];
		System.arraycopy(factors, 0, this.factors, 0, factors.length);
	}

	/**
	 * Return order of this polynomial.
	 * 
	 * @return Order.
	 */
	public short order() {
		return (short) (factors.length - 1);
	}

	/**
	 * Method multiply this factors of polynom with factors of polynom {@code p}
	 * and return results like {@code ComplexPolynomial}.
	 * 
	 * @param p
	 *            Multiplier.
	 * @return Results of multiplying.
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		int len = this.order() + p.order() + 1;
		Complex[] array = new Complex[len];

		int border = factors.length;
		int pBorder = p.factors.length;

		for (int i = 0; i < border; ++i) {
			for (int j = 0; j < pBorder; ++j) {
				if (array[i + j] == null) {
					array[i + j] = Complex.ZERO;
				}
				array[i + j] = array[i + j].add(factors[i].multiply(p.factors[j]));
			}
		}

		return new ComplexPolynomial(array);
	}

	/**
	 * Calculate first derivation of this polynomial.
	 * 
	 * @return Result of derivation.
	 */
	public ComplexPolynomial derive() {
		Complex[] array = new Complex[factors.length - 1];

		int i = 0;
		int border = factors.length;

		for (int j = 1; j < border; ++j, ++i) {
			array[i] = factors[j].multiply(new Complex(j, 0));
		}

		return new ComplexPolynomial(array);
	}

	/**
	 * Calculate complex number with variable {@code z}.
	 * 
	 * @param z
	 *            Variable in polynomial.
	 * @return Complex number.
	 */
	public Complex apply(Complex z) {
		Complex newComplex = Complex.ZERO;
		int border = factors.length;

		for (int i = 0; i < border; ++i) {
			newComplex = newComplex.add(factors[i].multiply(z.power(i)));
		}

		return newComplex;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int border = factors.length;

		for (int i = 0; i < border; ++i) {
			if (i > 1) {
				sb.append(" + (").append(factors[i].toString()).append(")*z^" + i);
			} else if (i == 0) {
				sb.append(factors[i].toString());
			} else {
				sb.append(" + (").append(factors[i].toString()).append(")*z");
			}

		}

		return sb.toString();
	}
}
