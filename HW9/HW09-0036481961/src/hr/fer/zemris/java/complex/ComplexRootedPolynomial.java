package hr.fer.zemris.java.complex;

/**
 * This class represent complex rooted polynomial with some operations.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ComplexRootedPolynomial {

	/**
	 * Represent array of complex polynomial numbers.
	 */
	private Complex[] roots;

	/**
	 * Constructor that receive {@code roots}.
	 * 
	 * @param roots
	 *            Roots of complex polynomial.
	 */
	public ComplexRootedPolynomial(Complex... roots) {

		this.roots = new Complex[roots.length];
		System.arraycopy(roots, 0, this.roots, 0, roots.length);
	}

	/**
	 * Calculate complex number with variable {@code z}.
	 * 
	 * @param z
	 *            Variable in rooted polynomial.
	 * @return Complex number.
	 */
	public Complex apply(Complex z) {
		ComplexPolynomial complexPolynomial = toComplexPolynom();
		return complexPolynomial.apply(z);
	}

	/**
	 * Method convert {@code ComplexRootedPolynomial} to
	 * {@code ComplexPolynomial}.
	 * 
	 * @return new {@code ComplexPolynomial}
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial complexPolynomial = new ComplexPolynomial(Complex.ONE);

		for (Complex root : roots) {
			complexPolynomial = complexPolynomial.multiply(new ComplexPolynomial(root.negate(), Complex.ONE));
		}
		return complexPolynomial;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int border = roots.length;

		for (int i = 0; i < border; ++i) {
			if (i > 1) {
				sb.append(" + (").append(roots[i].toString()).append(")*z^(1/" + i + ")");
			} else if (i == 0) {
				sb.append(roots[i].toString());
			} else {
				sb.append(" + (").append(roots[i].toString()).append(")*z");
			}

		}

		return sb.toString();
	}

	/**
	 * Method findes index of closest root for complex number {@code z} that is
	 * within {@code trshold} and return index of root, otherwise -1.
	 * 
	 * @param z
	 *            Complex number.
	 * @param trshold
	 *            Given trshold.
	 * @return Index of closest root or -1.
	 */
	public int indexOfClosestRootFor(Complex z, double trshold) {
		int border = roots.length;
		int indexOfsmallest = 0;
		double smallest = z.sub(roots[0]).module();

		for (int i = 1; i < border; ++i) {
			double check = z.sub(roots[i]).module();
			if (check < smallest) {
				smallest = check;
				indexOfsmallest = i;
			}
		}

		if (smallest > trshold) {
			return -1;
		}
		return indexOfsmallest;
	}

}
