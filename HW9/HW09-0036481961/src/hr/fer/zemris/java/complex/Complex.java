package hr.fer.zemris.java.complex;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.hypot;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

import java.text.DecimalFormat;

/**
 * This class represent Complex number with his operations.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Complex {

	/**
	 * Represent real part of complex number.
	 */
	private double re;

	/**
	 * Represent imaginary part of complex number.
	 */
	private double im;

	/**
	 * Represent zero in complex number.
	 */
	public static final Complex ZERO = new Complex(0, 0);

	/**
	 * Represent one in complex number.
	 */
	public static final Complex ONE = new Complex(1, 0);

	/**
	 * Represent negative one in complex number.
	 */
	public static final Complex ONE_NEG = new Complex(-1, 0);

	/**
	 * Represent i.
	 */
	public static final Complex IM = new Complex(0, 1);

	/**
	 * Represent -i.
	 */
	public static final Complex IM_NEG = new Complex(0, -1);

	/**
	 * Constructor receive real and imaginary part of complex number and set
	 * them on corresponding values.
	 * 
	 * @param re
	 *            Real part of complex number.
	 * @param im
	 *            Imaginary part of complex number.
	 */
	public Complex(double re, double im) {
		super();
		this.re = re;
		this.im = im;
	}

	/**
	 * Just initializes our complex number like ZERO.
	 */
	public Complex() {
		this(0, 0);
	}

	/**
	 * Complex calculate module of this complex number, with next formula:
	 * sqrt(re^2+im^2).
	 * 
	 * @return Module of our complex number.
	 */
	public double module() {
		return hypot(re, im);
	}

	/**
	 * Method multiply this complex number with complex number {@code c} and
	 * return results like complex number.
	 * 
	 * @param c
	 *            Multiplier.
	 * @return Results of multiplying.
	 */
	public Complex multiply(Complex c) {
		return new Complex((this.re * c.re - this.im * c.im), (this.im * c.re + this.re * c.im));
	}

	/**
	 * Method divide this complex number with complex number {@code c} and
	 * return results like complex number.
	 * 
	 * @param c
	 *            Divisor.
	 * @return Results of division.
	 */
	public Complex divide(Complex c) {
		double real = ((this.re * c.re) + (this.im * c.im)) / (pow(c.re, 2) + pow(c.im, 2));
		double imaginary = ((this.im * c.re) - (this.re * c.im)) / (pow(c.re, 2) + pow(c.im, 2));
		return new Complex(real, imaginary);
	}

	/**
	 * This method add complex number {@code c} to this complex number.
	 * 
	 * @param c
	 *            Addend.
	 * @return Sum.
	 */
	public Complex add(Complex c) {
		return new Complex(this.re + c.re, this.im + c.im);
	}

	/**
	 * This method take away complex number {@code c} from this complex number.
	 * 
	 * @param c
	 *            Subtrahend.
	 * @return Result of subtraction.
	 */
	public Complex sub(Complex c) {
		return new Complex(this.re - c.re, this.im - c.im);
	}

	/**
	 * Method return negation of this complex number. Method will return this ->
	 * (-{@code re}+(-{@code im})).
	 * 
	 * @return Negation of complex number.
	 */
	public Complex negate() {
		return new Complex(-re, -im);
	}

	/**
	 * This method calculate power of this complex number and return it like
	 * complex number. If {@code n} less then zero, method will throw
	 * {@code IllegalArgumentException}.
	 * 
	 * @param n
	 *            Potency.
	 * @return Complex number.
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Number n must be positive. You give this number: " + n);
		} else if (n == 0) {
			return ONE;
		} else if (n == 1) {
			return new Complex(re, im);
		} else {
			double r = pow(hypot(im, re), n);
			double fi = atan2(im, re);

			return new Complex(r * cos(fi * n), r * sin(fi * n));
		}
	}

	/**
	 * This method return {@code n}-th root of this complex number, {@code n} is
	 * positive number method will throw {@code IllegalArgumentException}.
	 * 
	 * @param n
	 *            Root of complex number.
	 * @return Array of complex numbers.
	 */
	public Complex[] root(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Number must be positive. You give this number: " + n);
		} else if (n == 0) {
			Complex[] root = { ONE };
			return root;
		}

		Complex[] roots = new Complex[n];

		if (n == 1) {
			roots[0] = new Complex(re, im);
		} else {
			double r = pow(hypot(im, re), 1.0 / n);
			double fi = atan2(im, re);

			for (int i = 0; i < n; ++i) {
				roots[i] = new Complex((r * cos((fi + 2 * i * PI) / n)), (r * sin((fi + 2 * i * PI) / n)));
			}
		}

		return roots;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.##");

		if (im < 0) {
			return (df.format(re) + " - " + df.format(abs(im)) + "i");
		} else {
			return (df.format(re) + " + " + df.format(im) + "i");
		}
	}

	/**
	 * Getter for real part of complex number.
	 * 
	 * @return Real part.
	 */
	public double getRe() {
		return re;
	}

	/**
	 * Getter for real imaginary of complex number.
	 * 
	 * @return Imaginary part.
	 */
	public double getIm() {
		return im;
	}
}
