package hr.fer.zemris.java.tecaj.hw07.crypto;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@SuppressWarnings({ "javadoc", "unused" })
public class HexToByteTest {

	@Test
	public void validHexStrings_1() {
		String string1 = "aBCd";
		String string2 = "AbCD";
		byte[] hex1 = EncryptDecrypt.hexToByte(string1);
		byte[] hex2 = EncryptDecrypt.hexToByte(string2);
		Assert.assertArrayEquals(hex1, hex2);
	}
	
	@Test
	public void validHexStrings_3() {
		String string1 = "ABCD";
		String string2 = "abcd";
		byte[] hex1 = EncryptDecrypt.hexToByte(string1);
		byte[] hex2 = EncryptDecrypt.hexToByte(string2);
		Assert.assertArrayEquals(hex1, hex2);
	}

	@Test
	public void validHexStrings_2() {
		String string1 = "a5";
		byte[] hex1 = EncryptDecrypt.hexToByte(string1);
		byte bajt = (byte) 165;
		Assert.assertArrayEquals(new byte[] { bajt }, hex1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidHexStrings_oddLength() {
		String string1 = "a53";
		byte[] hex1 = EncryptDecrypt.hexToByte(string1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidHexStrings_illegalCharacters() {
		String string1 = "a53z";
		byte[] hex1 = EncryptDecrypt.hexToByte(string1);
	}

}
