package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class use for encrypting or decrypting.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class EncryptDecrypt {

	/**
	 * Size of buffer
	 */
	private static final int BUFFER_SIZE = 4096;

	/**
	 * This method encrypt or decrypt provided input file, and then creates new
	 * file which name is provided.
	 * 
	 * @param command
	 *            Provided command. It could be encrypt or decrypt.
	 * @param srcFile
	 *            File for reading.
	 * @param outFile
	 *            File that must be crypted/decrypted.
	 */
	public static void cript(String command, String srcFile, String outFile) {
		System.out.printf("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\n>  ");

		Scanner input = new Scanner(System.in, "UTF-8");
		String keyText = input.nextLine();

		if (!isValidPassword(keyText)) {
			input.close();
			throw new IllegalArgumentException("Unesena je pogrešna šifra u hex-formatu.");
		}
		System.out.printf("Please provide initialization vector as hex-encoded text (32 hex-digits):\n>  ");
		String ivText = input.nextLine();

		if (!isValidPassword(ivText)) {
			input.close();
			throw new IllegalArgumentException("Unesena je pogrešna šifra u hex-formatu.");
		}

		input.close();

		SecretKeySpec keySpec = new SecretKeySpec(hexToByte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(hexToByte(ivText));

		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			boolean encrypt = false;

			if (command.matches("encrypt")) {
				encrypt = true;
			}

			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
			processing(cipher, srcFile, outFile);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Incijalizacija ciphera nije uspjela.");
		}
	}

	/**
	 * This method process encrypting or decrypting provided input file to
	 * output file.
	 * 
	 * @param cipher
	 *            For {@code ENCRYPT_MODE} or {@code DECRYPT_MODE}.
	 * @param srcFile
	 *            File for reading.
	 * @param outFile
	 *            File that must be crypted/decrypted.
	 */
	private static void processing(Cipher cipher, String srcFile, String outFile) {

		try (BufferedInputStream bufferIS = new BufferedInputStream(new FileInputStream(srcFile));) {

			BufferedOutputStream bufferOS = new BufferedOutputStream(new FileOutputStream(outFile));

			byte[] buffer = new byte[BUFFER_SIZE];
			while (true) {
				int result = bufferIS.read(buffer);

				if (result < 1) {
					break;
				}

				if (result < BUFFER_SIZE) {
					bufferOS.write(cipher.update(buffer, 0, result));
					bufferOS.write(cipher.doFinal());
				} else {
					bufferOS.write(cipher.update(buffer, 0, result));
				}
			}

			bufferIS.close();
			bufferOS.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Obrada podataka nije uspjela");
		}
		System.out.println("Decryption completed. Generated file " + outFile + " based on file " + srcFile + ".");
	}

	/**
	 * This method accepts hex-encoded {@code String} and returns appropriate
	 * {@code byte} array.
	 * 
	 * @param keyText
	 *            hex-encoded {@code String}.
	 * @return {@code byte} array.
	 */
	public static byte[] hexToByte(String keyText) {
		int len = keyText.length();

		if (len % 2 == 1) {
			throw new IllegalArgumentException("Length of string is odd!");
		}

		byte[] data = new byte[len / 2];

		for (int i = 0; i < len; i += 2) {

			if (!isHex(keyText.charAt(i)) || !isHex(keyText.charAt(i + 1))) {
				throw new IllegalArgumentException("Character " + keyText.charAt(i) + " is invalid!");
			}

			data[i / 2] = (byte) ((Character.digit(keyText.charAt(i), 16) << 4)
					+ Character.digit(keyText.charAt(i + 1), 16));
		}

		return data;
	}

	/**
	 * Method check is received password contains correct characters.
	 * 
	 * @param s
	 *            Represent password.
	 * @return True if password correct, otherwise false.
	 */
	private static boolean isValidPassword(String s) {
		return s.matches("[a-fA-F0-9]{32}");
	}

	/**
	 * Method check can received character be hexadecimal number.
	 * 
	 * @param c
	 *            Current character.
	 * @return True if it hexadecimal, otherwise false.
	 */
	private static boolean isHex(char c) {
		return String.valueOf(c).matches("[a-fA-F0-9]");
	}

}
