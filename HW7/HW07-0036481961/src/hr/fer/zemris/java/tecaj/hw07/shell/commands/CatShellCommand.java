package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Command cat takes one or two arguments. The first argument is path to some
 * file and is mandatory. The second argument is charset name that should be
 * used to interpret chars from bytes. If not provided, a default platform
 * charset will be used. This command opens given file and writes contents to
 * shell.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class CatShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of {@linkplain CatShellCommand}.
	 */
	public CatShellCommand() {
		super(CommandUtil.CAT, CommandUtil.CAT_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.isEmpty()) {
			throw new IllegalArgumentException("Wrong number of arguments provided!");
		}
		Charset charsetName = Charset.defaultCharset();
		String[] args = CommandUtil.checkCommandArguments(arguments);
		int beginIndex = 0;
		int endIndex = args[0].length();
		if (args[0].startsWith("\"") && args[0].endsWith("\"")) {
			beginIndex = args[0].indexOf("\"") + 1;
			endIndex = args[0].lastIndexOf("\"");
		}
		args[0] = args[0].substring(beginIndex, endIndex);

		if (args.length == 2) {
			try {
				charsetName = Charset.forName(args[1]);
			} catch (IllegalCharsetNameException | UnsupportedCharsetException ex) {
			}
		}
		Path p = Paths.get(args[0]);

		List<String> file;

		try {
			file = Files.readAllLines(p, charsetName);

			for (String line : file) {
				env.writeln(line);
			}
		} catch (IOException ex) {
		}
		return ShellStatus.CONTINUE;
	}

}
