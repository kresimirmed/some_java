package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class {@code ExitCommand} represents command EXIT. Exit command quits the
 * MyShell program (command interpreter).
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class ExitShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new {@code ExitCommand}.
	 */
	public ExitShellCommand() {
		super(CommandUtil.EXIT, CommandUtil.EXIT_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			env.writeln("Goodbye!");

		} catch (IOException e) {
		}
		return ShellStatus.TERMINATE;
	}

}
