package hr.fer.zemris.java.tecaj.hw07.shell;

import java.io.IOException;

/**
 * Interface {@code Environment} implements environment for my shall.
 * Environment is used for prints out messages from command, status...
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public interface Environment {
    /**
     * Reads line that user inputs
     * 
     * @return read line
     * @throws IOException
     *             If an I/O error occurs
     */
    String readLine() throws IOException;

    /**
     * Writes given text.
     * 
     * @param text
     *            text that needs to be printed out
     * @throws IOException
     *             If an I/O error occurs
     * 
     */
    void write(String text) throws IOException;

    /**
     * Prints a string and then terminates the line.
     * 
     * @param text
     *            text that needs to be printed out
     * @throws IOException
     *             If an I/O error occurs
     * 
     */
    void writeln(String text) throws IOException;

    /**
     * Returns iterator over {@code ShellCommand} commands.
     * 
     * @return iterator over {@code ShellCommand} commands.
     */
    Iterable<ShellCommand> commands();

    /**
     * Returns the multiline symbol.
     * 
     * @return the multiline symbol
     */
    public Character getMultilineSymbol();

    /**
     * Sets the multiline symbol to the provided character.
     * 
     * @param symbol
     *            the provided character.
     * @see #getMultilineSymbol()
     */
    public void setMultilineSymbol(Character symbol);

    /**
     * Returns the current prompt symbol.
     * 
     * @return the current prompt symbol.
     */
    public Character getPromptSymbol();

    /**
     * Sets the prompt symbol to the provided character.
     * 
     * @param symbol
     *            the provided character.
     * @see #getPromptSymbol()
     */
    public void setPromptSymbol(Character symbol);

    /**
     * Returns the morelines symbol.
     * 
     * @return the morelines symbol
     */
    public Character getMorelinesSymbol();

    /**
     * Sets the morelines symbol to the provided character.
     * 
     * @param symbol
     *            the provided character.
     * @see #getMorelinesSymbol()
     */
    public void setMorelinesSymbol(Character symbol);

    /**
     * Sets the symbol associated with the given name.
     * 
     * @param name
     *            the name of the symbol
     * @param newSymbol
     *            the value to set to
     * @throws IllegalArgumentException
     *             if the given symbol name is invalid
     */
    public void setSymbol(String name, Character newSymbol);

    /**
     * Gets the symbol associated with the given name.
     * 
     * @param name
     *            the name of the symbol
     * @return the symbol associated with the given name.
     * @throws IllegalArgumentException
     *             if the given symbol name is invalid
     */
    public Character getSymbol(String name);

}