package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class {@code HelpCommand} represents command HELP. Provides Help information
 * for {@code MyShell} commands.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class HelpShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of {@linkplain HelpShellCommand}.
	 */
	public HelpShellCommand() {
		super(CommandUtil.HELP, CommandUtil.HELP_DESCRIPTION);
	}

	/**
	 * Executes command HELP. If there is no arguments for this command this
	 * method will print out informations about all commands that can be used.
	 * 
	 * @param arguments
	 *            name of specified command
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argumentsArray = CommandUtil.checkCommandArguments(arguments);
		try {
			if (argumentsArray.length > 1) {
				env.writeln("Only one or zero arguments can be passed to HELP command");
				return ShellStatus.CONTINUE;
			}

			Iterable<ShellCommand> commands = env.commands();
			if (argumentsArray.length == 0) {
				allCommandsInfoPrint(env, commands);
			} else {
				specifiedCommandInfoPrint(env, commands, argumentsArray[0].toUpperCase());
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * Prints out in environment informations about all given commands.
	 * 
	 * @param environment
	 *            environment
	 * 
	 * @param commands
	 *            commands whose informations needs to be printed
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private void allCommandsInfoPrint(Environment environment, Iterable<ShellCommand> commands) throws IOException {
		for (ShellCommand command : commands) {
			environment.writeln(command.getCommandName() + ": " + command.getCommandDescription());
		}
	}

	/**
	 * Prints out in environment information about specified command who's name
	 * is passed.
	 * 
	 * @param environment
	 *            environment
	 * @param commands
	 *            iterator over commands
	 * @param commandName
	 *            command name
	 * @throws IOException
	 *             If an I/O error occurs
	 * 
	 */
	private void specifiedCommandInfoPrint(Environment environment, Iterable<ShellCommand> commands, String commandName)
			throws IOException {

		for (ShellCommand command : commands) {
			if (command.getCommandName().equals(commandName)) {
				environment.writeln((command.getCommandDescription()));
				return;
			}
		}
		environment.writeln("Command \"" + commandName + "\" thoes doesn't exists!");
	}
}
