package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class {@code MkdirCommand} represents command MKDIR in MyShell. MKDIR command
 * takes directory name and creates appropriate directory.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class MkdirShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of {@linkplain MkdirShellCommand}.
	 */
	public MkdirShellCommand() {
		super(CommandUtil.MKDIR, CommandUtil.MKDIR_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] args = CommandUtil.checkCommandArguments(arguments);
		if (args.length != 1) {
			throw new IllegalArgumentException("You provided wrong arguments of mkdir command!");
		}
				
		Path p = Paths.get(args[0]);
		
		File newFile = p.toFile();
		try {
			if (!newFile.exists()) {
				if (newFile.mkdirs()) {
					env.writeln("Directory created successful.");
				} else {
					env.writeln("An error occurs with directory creation.");
				}
			}
		} catch (Exception ex) {}
		
		return ShellStatus.CONTINUE;
	}

}
