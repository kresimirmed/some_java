package hr.fer.zemris.java.tecaj.hw07.shell;

/**
 * Enumeration u that represents status of command.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public enum ShellStatus {
	/**
	 * Continue with program.
	 */
	CONTINUE,

	/**
	 * Terminate program.
	 */
	TERMINATE;
}
