package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * This class use for calculates SHA checkSum, and checks provided checkSum and
 * checkSum which is calculated.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Sha {

	/**
	 * Size of buffer.
	 */
	private static final int BUFFER_SIZE = 4096;

	/**
	 * This is only method in this class and her job is to check SHA.
	 * 
	 * @param path
	 *            Represents path to the given file.
	 * @throws NoSuchAlgorithmException
	 *             If no Provider supports a MessageDigestSpi implementation for
	 *             the specified algorithm.
	 * @throws IOException
	 *             If the methods are invoked on the resulting stream to attempt
	 *             I/O on the stream.
	 */
	public static void checkSha(String path) throws NoSuchAlgorithmException, IOException {
		System.out.printf("Please provide expected sha signature for " + path + ":\n>  ");
		Scanner input = new Scanner(System.in, "UTF-8");
		String shacode = input.nextLine();
		input.close();
		MessageDigest messDigest = MessageDigest.getInstance("SHA-256");
		try (InputStream fileIS = new BufferedInputStream(new FileInputStream("./" + path))) {

			byte[] buffer = new byte[BUFFER_SIZE];

			while (true) {
				int result = fileIS.read(buffer);
				if (result < 1) {
					break;
				}
				messDigest.update(buffer, 0, result);
			}
		}

		String calculatedCheckSum = String.format("%064x", new BigInteger(1, messDigest.digest()));

		if (shacode.equals(calculatedCheckSum)) {
			System.out.println("Digesting completed. Digest of " + path + " matches " + "expected digest.");

		} else {
			System.out.println("Digesting completed. Digest of " + path + " does not "
					+ "match the expected digest.\nDigest was " + calculatedCheckSum);
		}
	}

}
