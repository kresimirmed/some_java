package hr.fer.zemris.java.tecaj.hw07.shell.commands;

/**
 * This class contains namess of all commands that can be used in
 * {@code MyShell}
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class CommandUtil {
	/**
	 * Regex used for splitting user input
	 */
	private static String SPLIT_REGEX = "[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

	/**
	 * Name of command HELP
	 */
	public static final String HELP = "HELP";
	/**
	 * Description of HELP command
	 */
	public static final String HELP_DESCRIPTION = "Provides Help information for MyShell commands.";
	/**
	 * Name of command EXIT
	 */
	public static final String EXIT = "EXIT";
	/**
	 * Description of EXIT command
	 */
	public static final String EXIT_DESCRIPTION = "Quits the MyShell program (command interpreter).";
	/**
	 * Name of command TREE
	 */
	public static final String TREE = "TREE";
	/**
	 * Description of TREE command
	 */
	public static final String TREE_DESCRIPTION = " Graphically displays the directory structure of a drive or path.";
	/**
	 * Name of command CHARSETS
	 */
	public static final String CHARSETS = "CHARSETS";
	/**
	 * Description of CHARSETS command
	 */
	public static final String CHARSETS_DESCRIPTION = "Lists names of supported charsets for your Java platform.";
	/**
	 * Name of command SYMBOL
	 */
	public static final String SYMBOL = "SYMBOL";
	/**
	 * Description of CHARSETS command
	 */
	public static final String SYMBOL_DESCRIPTION = "Sets new symbol for prompt, morilines or multi lines. For change symbol "
			+ "use it like this: SYMBOL {symbolName} newSymbol. If newySimbol is not inputed info about current symbol will be printed.";
	/**
	 * Name of command CAT
	 */
	public static final String CAT = "CAT";
	/**
	 * Description of CAT command
	 */
	public static final String CAT_DESCRIPTION = "Command cat takes one or two arguments. The first argument is path to some"
			+ " file and is mandatory. The second argument is charset name that should be used to interpret chars from bytes. "
			+ "If not provided, a default platform charset will be used. This command opens given file and writes its content to shell.";
	/**
	 * Name of command LS
	 */
	public static final String LS = "LS";
	/**
	 * Description of LS command
	 */
	public static final String LS_DESCRIPTION = "Prints info about all files in directory";
	/**
	 * Name of command COPY
	 */
	public static final String COPY = "COPY";
	/**
	 * Description of LS command
	 */
	public static final String COPY_DESCRIPTION = "Copys one file from one location to another.";
	/**
	 * Name of command MKDIR
	 */
	public static final String MKDIR = "MKDIR";
	/**
	 * Description of MKDIR command
	 */
	public static final String MKDIR_DESCRIPTION = "Takes directory name nad creates appropriate directory.";
	/**
	 * Name of command HEXDUMP
	 */
	public static final String HEXDUMP = "HEXDUMP";
	/**
	 * Description of MKDIR command
	 */
	public static final String HEXDUMP_DESCRIPTION = "Display specified file or input in hexadecimal";

	/**
	 * Check argument for command whose name is passed. If environment is null
	 * {@code NullPointerException} will be thrown. Method will return array of
	 * valid or not valid arguments for specified command.
	 * 
	 * @param arguments
	 *            Command arguments
	 * @return Array of arguments
	 */
	public static String[] checkCommandArguments(String arguments) {
		arguments = arguments.replaceAll("\\s+", " ").trim();

		if (arguments.trim().isEmpty()) {
			return new String[0];
		}

		String[] argumentsArray = arguments.split(SPLIT_REGEX);

		for (int i = 0; i < argumentsArray.length; i++) {
			String arg = argumentsArray[i];

			if (arg.startsWith("\"") && arg.endsWith("\"")) {
				argumentsArray[i] = arg.substring(1, arg.length() - 1);
			}
		}

		return argumentsArray;
	}
}
