package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class {@code HexDumpCommand} represents HEXDUMP command in {@code MyShell}.
 * Display specified file or input in hexadecimal.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class HexDumpShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of {@linkplain HexDumpShellCommand}
	 */
	public HexDumpShellCommand() {
		super(CommandUtil.HEXDUMP, CommandUtil.HEXDUMP_DESCRIPTION);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] argumentsArray = CommandUtil.checkCommandArguments(arguments);

		if (argumentsArray.length != 1) {
			throw new IllegalArgumentException("Command HEXDUMP takes only one argument!");
		}

		Path file = Paths.get(argumentsArray[0]);
		try {
			byte[] bytes = Files.readAllBytes(file);
			printBytes(env, bytes);
		} catch (IOException exception) {
			throw new IllegalArgumentException("Error while reading file!");
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * Prints given array of bytes in formated way.
	 * 
	 * @param environment
	 *            environment
	 * @param bytes
	 *            array of bytes
	 * @throws IOException
	 *             if I/O error occurs while writing to the environment
	 */
	private void printBytes(Environment environment, byte[] bytes) throws IOException {

		String hexLine = null;
		String textLine = "";

		int size = bytes.length;
		for (int i = 0; i < size; i++) {

			if (i % 16 == 0) {

				if (hexLine != null) {
					environment.writeln(hexLine + textLine);
				}

				hexLine = String.format("%08x", i) + ": ";
				textLine = "";
			}

			byte current = bytes[i];
			if (current <= 15 && current >= 0) {
				hexLine += "0";
			}
			hexLine += Integer.toString(current, 16) + " ";

			if (current >= 32 && current <= 127) {
				textLine += (char) current;

			} else {
				textLine += '.';
			}
			if ((i % 8 == 0 || (i + 1) % 16 == 0) && i % 16 != 0) {
				hexLine += "| ";
			}
		}
		int index = size % 16;
		if (index != 0) {
			for (; index < 16; index++) {
				hexLine += "   ";

				if ((index + 1) % 16 == 0) {
					hexLine += " ";
				}
				if ((index % 8 == 0 || (index + 1) % 16 == 0) && index % 16 != 0) {
					hexLine += "|";
				}

			}
			environment.writeln(hexLine + " " + textLine);
		}
	}

}
