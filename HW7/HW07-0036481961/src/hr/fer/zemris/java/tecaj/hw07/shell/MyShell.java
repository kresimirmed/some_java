package hr.fer.zemris.java.tecaj.hw07.shell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.shell.commands.*;

/**
 * This class reprezent shell,it is some kind of CMD imitation.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class MyShell {
	/**
	 * Instance of environment.
	 */
	public static Environment environment = new EnvironmentImpl();

	/**
	 * This is main method, it start with program.
	 * 
	 * @param args
	 *            Unused.
	 * @throws IOException
	 *             if an IO error occurs.
	 */
	public static void main(String[] args) throws IOException {

		CommandExecutor executor = new CommandExecutor(environment);

		environment.writeln("Welcome to MyShell! v 1.0");
		while (true) {
			System.out.print(getPrompt());

			try {
				ShellStatus shellStatus = executor.executeCommand();

				if (shellStatus == ShellStatus.TERMINATE) {
					break;
				}
			} catch (IllegalArgumentException exception) {
				environment.writeln(exception.getMessage());
				continue;
			}
		}

		System.out.println("Goodbye!");
	}

	/**
	 * Returns prompt symbol and one white space.
	 * 
	 * @return prompt symbol + whitespace
	 */
	private static String getPrompt() {
		return environment.getPromptSymbol() + " ";
	}

	/**
	 * Implementation of environment. Default prompt symbol is '>'.
	 * 
	 * @author Krešimir Medvidović
	 *
	 */
	public static class EnvironmentImpl implements Environment {

		/**
		 * Default prompt symbol is '>'
		 */
		public static final Character DEFAULT_PROMPT = '>';
		/**
		 * Default multiline symbol is '|'
		 */
		public static final Character DEFAULT_MULTILINE = '|';
		/**
		 * Default multiline symbol is '\'
		 */
		public static final Character DEFAULT_MORELINE = '\\';

		/**
		 * A reader that reads from the standard input
		 */
		private BufferedReader reader;

		/**
		 * Symbol for prompt.
		 */
		private Character prompt;

		/**
		 * Symbol for multi line.
		 */
		private Character multiLine;

		/**
		 * Symbol for more lines.
		 */
		private Character moreLine;
		/**
		 * 
		 */
		private List<ShellCommand> listOfCommands;

		/**
		 * Constructs new environment.
		 */
		public EnvironmentImpl() {
			reader = new BufferedReader(new InputStreamReader(System.in));
			prompt = DEFAULT_PROMPT;
			moreLine = DEFAULT_MORELINE;
			multiLine = DEFAULT_MULTILINE;
			listOfCommands = new ArrayList<>();

			listOfCommands.add(new ExitShellCommand());
			listOfCommands.add(new HelpShellCommand());
			listOfCommands.add(new CharsetsShellCommand());
			listOfCommands.add(new CatShellCommand());
			listOfCommands.add(new CopyShellCommand());
			listOfCommands.add(new HexDumpShellCommand());
			listOfCommands.add(new LsShellCommand());
			listOfCommands.add(new MkdirShellCommand());
			listOfCommands.add(new SymbolShellCommand());
			listOfCommands.add(new TreeShellCommand());
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getPromptSymbol() {
			return prompt;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setPromptSymbol(Character promptSymbol) {
			this.prompt = promptSymbol;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getMultilineSymbol() {
			return multiLine;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setMultilineSymbol(Character multilineSymbol) {
			this.multiLine = multilineSymbol;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getMorelinesSymbol() {
			return moreLine;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setMorelinesSymbol(Character morelinesSymbol) {
			this.moreLine = morelinesSymbol;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String readLine() throws IOException {
			return reader.readLine();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void write(String text) throws IOException {
			System.out.print(text);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void writeln(String text) throws IOException {
			System.out.println(text);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Iterable<ShellCommand> commands() {
			return listOfCommands;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getSymbol(String name) {
			if (name.equalsIgnoreCase("prompt")) {
				return prompt;
			} else if (name.equalsIgnoreCase("morelines")) {
				return moreLine;
			} else if (name.equalsIgnoreCase("multiline")) {
				return multiLine;
			}

			throw new IllegalArgumentException("Invalid symbol name. You write: " + name);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setSymbol(String name, Character newSymbol) {
			if (name.equalsIgnoreCase("PROMPT")) {
				prompt = newSymbol;
			} else if (name.equalsIgnoreCase("MORELINES")) {
				moreLine = newSymbol;
			} else if (name.equalsIgnoreCase("MULTILINE")) {
				multiLine = newSymbol;
			} else {
				throw new IllegalArgumentException("Invalid symbol name. Yout write: " + name);
			}
		}

	}
}
