package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Class that is used to test code generated from the data, encrypting or
 * decripting data (given file) using the AES cryptoalgorithm and the 128-bit
 * encryption key or calculate and check the SHA-256 file digest.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class Crypto {

	/**
	 * Main method it start whit this class, used to call the appropriate method
	 * and function.
	 * 
	 * @param args
	 *            arguments from command line. Program accepts two or three
	 *            parameters. First parameter is operation, and second parameter
	 *            is name of file on which provided operation will be executed.
	 * @throws NoSuchAlgorithmException
	 *             Use for method {@code chackSha}, look in her java-doc.
	 * @throws IOException
	 *             Use for method {@code chackSha}, look in her java-doc.
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException, IOException {

		if (args.length < 2 || args.length > 5) {
			throw new IllegalArgumentException("Provided number of arguments is invalid!");
		}
		if (args[0].matches("checksha")) {
			Sha.checkSha(args[1]);
		} else if (args[0].matches("encrypt?") || args[0].matches("decrypt?") && args.length == 3) {
			EncryptDecrypt.cript(args[0], args[1], args[2]);
		} else {
			throw new RuntimeException("Arguments are invalid. Keywords are checksaha, encript or decrypt.");
		}

	}
}
