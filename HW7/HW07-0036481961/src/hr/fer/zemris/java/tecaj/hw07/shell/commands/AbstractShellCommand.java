package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;

/**
 * Superclass of all commands.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public abstract class AbstractShellCommand implements ShellCommand {
	
    /**
     * Command name
     */
    private String commandName;
    
    /**
     * Command description
     */
    private String commandDescription;

    /**
     * Constructs new command with given name and description.
     * 
     * @param commandName
     *            Command name
     * @param commandDescription
     *            Command description
     */
    public AbstractShellCommand(String commandName, String commandDescription) {
	this.commandName = commandName;
	this.commandDescription = commandDescription;
    }

    /**
     * Method returns name.
     * 
     * @return {@code commandName}
     */
    public String getCommandName() {
	return commandName;
    }

    /**
     * Method returns description.
     * 
     * @return {@code commandDescription}
     */
    public String getCommandDescription() {
	return commandDescription;
    }

}
