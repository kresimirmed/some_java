package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class {@code SymbolCommand} Command sets new symbol for prompt, morelines or
 * multilines.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 */
public class SymbolShellCommand extends AbstractShellCommand {

    /**
     * Constructs new instance of {@linkplain SymbolShellCommand}
     */
    public SymbolShellCommand() {
	super(CommandUtil.SYMBOL, CommandUtil.SYMBOL_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
	String[] parsedArguments = CommandUtil.checkCommandArguments(arguments);
	int size = parsedArguments.length;

	if (size < 1 && size > 2) {
	    throw new IllegalArgumentException("At last one or two arguments must be passed!");
	}

	String symbolName = parsedArguments[0].toUpperCase();
	Character currentSymbol = env.getSymbol(symbolName);

	try {
	    if (size == 1) {
		env.writeln("Symbol for " + symbolName + " is " + currentSymbol);
	    }
	    if (size == 2) {
		String newSymbol = parsedArguments[1].trim();

		if (newSymbol.length() != 1) {
		    throw new IllegalArgumentException("Symbol must be only one character!");
		}

		Character prewSymbol = currentSymbol;
		env.setSymbol(symbolName, newSymbol.charAt(0));

		env.writeln("Symbol for " + symbolName + " changed from " + prewSymbol
			+ " to " + newSymbol);
	    }
	} catch (IOException exception) {
	    exception.printStackTrace();
	}

	return ShellStatus.CONTINUE;
    }

}
