package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class represents TREE command.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class TreeShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of TreeCommand.
	 */
	public TreeShellCommand() {
		super(CommandUtil.TREE, CommandUtil.TREE_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] args = CommandUtil.checkCommandArguments(arguments);
		if (args.length != 1) {
			throw new IllegalArgumentException("You provided wrong arguments of tree command!");
		}
		int beginIndex = 0;
		int endIndex = args[0].length();
		if (args[0].startsWith("\"") && args[0].endsWith("\"")) {
			beginIndex = args[0].indexOf("\"") + 1;
			endIndex = args[0].lastIndexOf("\"");
		}
		args[0] = args[0].substring(beginIndex, endIndex);
		Path p = Paths.get(args[0]);

		if (!Files.isDirectory(p)) {
			throw new IllegalArgumentException("You provided path of file!");
		}

		try {
			Files.walkFileTree(p, new Print(env));
		} catch (IOException ex) {
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method print out.
	 * 
	 * @author Krešimir Medvidović
	 *
	 */
	private static class Print implements FileVisitor<Path> {

		/**
		 * Represent level.
		 */
		private int level;

		/**
		 * Represent Environment.
		 */
		private Environment env;

		/**
		 * Print Environment
		 * 
		 * @param env
		 *            Environment
		 */
		public Print(Environment env) {
			this.env = env;
		}

		/**
		 * Print path.
		 * 
		 * @param f
		 *            Path
		 */
		private void print(Path f) {
			try {
				if (level == 0) {
					env.writeln(f.normalize().toAbsolutePath().toString());

				} else {
					env.writeln(String.format("%" + (2 * level) + "s%s%n", "", f.normalize().getFileName()));
				}
			} catch (IOException ex) {
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			print(dir);
			level++;
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			print(file);
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			level--;
			return FileVisitResult.CONTINUE;
		}
	}
}
