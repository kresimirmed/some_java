package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Lists names of supported charsets for your Java platform.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class CharsetsShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new {@code CharsetsCommand}
	 */
	public CharsetsShellCommand() {
		super(CommandUtil.CHARSETS, CommandUtil.CHARSETS_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment environment, String arguments) {

		String[] argumentsArray = CommandUtil.checkCommandArguments(arguments);

		try {
			if (argumentsArray.length != 0) {
				environment.writeln("Command \"CHARSETS\" requires no arguments.");
				return ShellStatus.CONTINUE;
			}

			Map<String, Charset> map = Charset.availableCharsets();
			for (Map.Entry<String, Charset> pair : map.entrySet()) {
				environment.writeln(((Charset) pair.getValue()).name());
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return ShellStatus.CONTINUE;
	}
}
