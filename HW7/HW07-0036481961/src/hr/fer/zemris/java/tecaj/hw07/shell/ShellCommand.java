package hr.fer.zemris.java.tecaj.hw07.shell;

/**
 * This interface models a commands used in MyShell.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public interface ShellCommand {
	/**
	 * Returns the name of the command.
	 * 
	 * @return Command name.
	 */
	String getCommandName();

	/**
	 * Returns a description.
	 * 
	 * @return command description.
	 */
	String getCommandDescription();

	/**
	 * This method executes command
	 * 
	 * @param env
	 *            Represent environment.
	 * @param arguments
	 *            String which represents everything that user entered AFTER the
	 *            command name.
	 * @return Command status, if command status is TERMINATE environment will
	 *         terminate {@code MyShell} and if is CONTINUE environment will
	 *         wait for next command.
	 * @throws NullPointerException
	 *             If environment is {@code null}
	 */
	ShellStatus executeCommand(Environment env, String arguments);
}
