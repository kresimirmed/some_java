package hr.fer.zemris.java.tecaj.hw07.shell;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Reads input from shell and executes command
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class CommandExecutor {
	/**
	 * Instance of environment
	 */
	private Environment environment;
	/**
	 * Map of command that are supported in this shell
	 */
	private static Map<String, ShellCommand> commands;
	/**
	 * Name of current command
	 */
	private String commandName = null;

	/**
	 * Constructs new instance of {@code CommandExecutor}
	 * 
	 * @param environment
	 *            environment
	 */
	public CommandExecutor(Environment environment) {
		this.environment = environment;
		commands = new HashMap<>();
		Iterable<ShellCommand> cc = environment.commands();
		for (ShellCommand c : cc) {
			commands.put(c.getCommandName(), c);
		}

	}

	/**
	 * Executes next command that user provides.
	 * 
	 * @return {@code ShellStatus} CONTINUE or TERMINATE
	 * @throws IOException
	 */
	public ShellStatus executeCommand() throws IOException {
		String input = environment.readLine();

		ShellCommand command = getCommand(input);
		String arguments = getArgument(input);

		return command.executeCommand(environment, arguments);
	}

	/**
	 * Return argument that user provide for command.
	 * 
	 * @param firstLine
	 *            User first line input.
	 * @return Argument for command
	 * @throws IOException
	 *             If an IO error occurs.
	 */
	private String getArgument(String firstLine) throws IOException {
		firstLine = firstLine.trim();

		if (firstLine.equals(commandName)) {
			return "";
		}

		firstLine = firstLine.substring(commandName.length());

		StringBuilder stringBuilder = new StringBuilder();
		commandName = null;
		String input = firstLine.trim();
		boolean end = true;

		while (true) {
			input = input.trim();
			end = true;

			if (input.endsWith("" + environment.getMorelinesSymbol())) {
				input = input.substring(0, input.length() - 1).trim();
				end = false;
			}

			stringBuilder.append(input);

			if (end) {
				return stringBuilder.toString();
			} else {
				environment.write(environment.getMultilineSymbol() + " ");
				input = environment.readLine();
			}

		}
	}

	/**
	 * Returns command that user input
	 * 
	 * @param input
	 *            user's input
	 * @return command
	 * @throws IllegalArgumentException
	 *             if user provide invalid command
	 */
	private ShellCommand getCommand(String input) {
		input = input.trim();
		commandName = input.split(" ")[0].toUpperCase();
		if (commands.containsKey(commandName)) {
			return commands.get(commandName);
		} else {
			throw new IllegalArgumentException(
					"Command " + commandName + " is unsupported! Type HELP to see all supported commands.");
		}
	}
}
