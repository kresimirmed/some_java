package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * This class represents COPY command in {@code MyShell}. COPY command copys one
 * file from one location to another.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class CopyShellCommand extends AbstractShellCommand {

	/**
	 * Constructs new instance of {@linkplain CopyShellCommand}.
	 */
	public CopyShellCommand() {
		super(CommandUtil.COPY, CommandUtil.COPY_DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.isEmpty()) {
			throw new IllegalArgumentException("");
		}
		String[] args = CommandUtil.checkCommandArguments(arguments);

		args[0] = extractFromArgs(args[0]);
		args[1] = extractFromArgs(args[1]);

		if (args.length != 2) {
			throw new IllegalArgumentException("Wrong number of arguments!");
		}

		Path source = Paths.get(args[0]);
		Path dest = Paths.get(args[1]);

		if (!Files.exists(source) || Files.isDirectory(source)) {
			throw new IllegalArgumentException("Provided path is directory or doesn't exist!");
		}
		String overwrite = null;

		if (!Files.isDirectory(dest) && Files.exists(dest)) {
			try {
				env.write("Destination file alredy exist, do you want to overwrite it? [y/n]: ");
				overwrite = env.readLine().toUpperCase().trim();
			} catch (IOException ex) {
			}

			if (overwrite.equals("N")) {
				throw new IllegalArgumentException("Datoteka nije kopirana.");
			} else if (!overwrite.equals("y")) {
				throw new IllegalArgumentException("You typed wrong letter!");
			}
		}

		byte[] buffer = new byte[4096];
		int result = 0;
		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(source.toFile()));
				BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(dest.toFile()))) {
			while ((result = input.read(buffer)) != -1) {
				output.write(buffer, 0, result);
			}
		} catch (IOException ex) {
		}

		try {
			env.writeln("Files copied successful!");
		} catch (IOException e) {
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * Remove characters (").
	 * 
	 * @param arg
	 *            Received String.
	 * @return return String without ".
	 */
	private static String extractFromArgs(String arg) {
		int beginIndex = 0;
		int endIndex = arg.length();
		if (arg.startsWith("\"") && arg.endsWith("\"")) {
			beginIndex = arg.indexOf("\"") + 1;
			endIndex = arg.lastIndexOf("\"");
		}
		return arg.substring(beginIndex, endIndex);
	}

}
