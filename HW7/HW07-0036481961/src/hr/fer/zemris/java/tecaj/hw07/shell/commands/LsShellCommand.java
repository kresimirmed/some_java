package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Class represents {@code LS} command in shell. This command takes single
 * argument (directory) and prints out informations about all files in it.
 * 
 * @author Krešimir Medvidović
 * @version 1.0
 *
 */
public class LsShellCommand extends AbstractShellCommand {

    /**
     * Constructs new instance of {@linkplain LsShellCommand}.
     */
    public LsShellCommand() {
	super(CommandUtil.LS, CommandUtil.LS_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
    	
	String[] argumentsArray = CommandUtil.checkCommandArguments(arguments);
	int size = argumentsArray.length;

	if (size != 1) {
	    throw new IllegalArgumentException("Command LS takes only one argument!");
	}

	Path file = Paths.get(argumentsArray[0]);
	try {
	    Stream<Path> child = Files.list(file);
	    child.forEach((path) -> formatPrint(env, path));
	    child.close();
	} catch (IOException exception) {
	    throw new IllegalArgumentException("Path to directory is not valid!");
	}
	return ShellStatus.CONTINUE;
    }

    /**
     * Prints file informations in formated way.
     * 
     * @param environment
     *            environment
     * @param path
     *            path of file
     */
    private void formatPrint(Environment environment, Path path) {
	char directory = '-', read = '-', write = '-', execut = '-';

	try {
	    if (Files.isDirectory(path)) {
		directory = 'd';
	    }

	    if (Files.isReadable(path)) {
		read = 'r';
	    }
	    if (Files.isWritable(path)) {
		write = 'w';
	    }
	    if (Files.isExecutable(path)) {
		execut = 'x';
	    }
	    String firstC = "" + directory + read + write
		    + execut;


	    String fileSize = String.format("%1$10d", Files.size(path));


	    SimpleDateFormat simpleDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    BasicFileAttributeView faileView = Files.getFileAttributeView(path,
		    BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
	    BasicFileAttributes attributes = faileView.readAttributes();
	    FileTime fileTime = attributes.creationTime();
	    String dataTime = simpleDF
		    .format(new Date(fileTime.toMillis()));


	    String fileName = path.getFileName().toString();

	    environment.writeln(firstC + " " + fileSize + " " + dataTime
		    + " " + fileName);

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
